# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import bmesh
import os
import numpy as np
import time
from mathutils import Matrix, Vector

from ..common.geometry_3D import print_m
from ..common.gp_utils import get_layer_collection
from ..common import mode_manager
from ..common import log_3D

from .. import core
from .projection_clusters import get_camera_bounds
from .render import get_all_visible_GP_Objects
from .data_export import get_export_path, get_project_name



last_asset_update_time = None
last_asset_list = None



def get_png_color_depth(image_path):
    """
    Prend le chemin d'une image PNG et retourne la profondeur de couleur en bits par couche.
    
    Args:
        image_path (str): Le chemin vers le fichier PNG.
    
    Returns:
        int: La profondeur de couleur en bits par couche (8 ou 16).
    """
    try:
        with open(image_path, "rb") as f:
            # Vérifier la signature PNG
            signature = f.read(8)
            if signature != b'\x89PNG\r\n\x1a\n':
                raise ValueError("Ce fichier n'est pas un PNG valide.")
            
            # Lire le chunk IHDR (premier chunk contenant les métadonnées)
            ihdr_length = int.from_bytes(f.read(4), "big")  # Taille du chunk IHDR
            ihdr_type = f.read(4)
            if ihdr_type != b'IHDR':
                raise ValueError("Le fichier PNG est corrompu ou mal formé (IHDR manquant).")
            
            # Lire les données de l'entête IHDR
            ihdr_data = f.read(ihdr_length)
            bit_depth = ihdr_data[8]  # Le 9e octet (index 8) contient la profondeur en bits par couche
            
            return bit_depth
    except Exception as e:
        print(f"Erreur lors de l'analyse du fichier PNG : {e}")
        return None
    

def update_viewers(context):
    """Forces viewer update, could be necessary after updating texture.

    Parameters:
    - context (bpy.types.Context): The Blender context."""

    if context.scene.render.engine not in ['BLENDER_EEVEE','BLENDER_WORKBENCH']:
        wman = bpy.data.window_managers['WinMan']
        for win in wman.windows :
            for area in win.screen.areas :
                if area.type=='VIEW_3D' :
                    for space in area.spaces :
                        if space.type == 'VIEW_3D' and space.shading.type == 'RENDERED' :
                            space.shading.type = 'SOLID'
                            space.shading.type = 'RENDERED'

def reload_modified_images():
    """Forces refresh of textures."""

    for item in bpy.data.images:
        if not item.library and not item.packed_file and item.source not in {'VIEWER','GENERATED'} :
            path = os.path.abspath(bpy.path.abspath(item.filepath))
            if os.path.isfile(path):
                item.reload()


def get_texture_planes_images() :
    assert "COL-GP" in bpy.data.collections, "Could not find COL-GP collection."
    col_gp = bpy.data.collections["COL-GP"]

    meshes = [o for o in col_gp.all_objects if type(o.data).__name__ == "Mesh"]
    images = []

    for o in meshes :
        nodes = o.active_material.node_tree.nodes
        for node in nodes : 
            if node.bl_idname == "ShaderNodeTexImage" : 
                img = node.image
                conditions = [
                    not img.library,
                    not img.packed_file,
                    img.source not in {'VIEWER','GENERATED'},
                ]

                if False not in conditions : 
                    images.append(img)

    return images


def sanity_check_image_asset_size() :
    """Checks if every loaded image texture originating from a -assets folder 
    has the same width and height, else returns an error message to be displayed."""

    images = get_texture_planes_images()
    
    sizes = []
    object_names = []
    paths = []
    depths = []
    
    for item in images :
        name = bpy.path.basename(item.filepath)
        sizes.append((item.size[0], item.size[1]))
        object_names.append(name)
        paths.append(item.filepath)
        depths.append(get_png_color_depth(item.filepath))

    assert len(sizes) > 0, "Refresh : no valid texture was found to be refreshed, please recheck your PSD link."

    size_data = np.array(sizes)
    median = np.median(size_data, axis=0).tolist()
    median = [int(i) for i in median]
    
    if any([any([sz > 10000 for sz in size]) for size in sizes]) and not all([d == 16 for d in depths]): 
        return f"This scene is using big textures (>10000px), they must be exported from photoshop with the script at {os.path.join(os.path.dirname(__file__), 'exportassets_from_active_doc.jsx')}"
        
    for i,s in enumerate(sizes) :
        if s[0] == 0 and s[1] == 0 :
            return f"Unable to load texture : {paths[i]}."
        
        elif median[0] != s[0] or median[1] != s[1] :
            pp = lambda v : "%dx%d" % (v[0], v[1])
            return f"Size of image asset '{object_names[i]}' is incorrect : \n{pp(s)} instead of {pp(median)}"


def list_asset_folders() :
    """Deprecated, should be deleted on next refactor."""

    path = get_export_path()
    assets_folders = []
    
    for f in os.listdir(path) :
        if os.path.isdir(os.path.join(path, f)) and "-assets" in f :
            assets_folders.append(f)
    
    return assets_folders

def get_current_asset_folder() :

    return bpy.context.scene.assets_pack_list


def update_image_asset_folder() :
    """Updates the file paths of image assets based on the current asset folder."""

    for item in bpy.data.images:
        if "-assets" in item.filepath :
            if item.packed_file :
                print("Image is packed, unpacking.")
                item.unpack(method="USE_LOCAL")

            path = os.path.dirname(item.filepath)
            asset_folder = os.path.basename(path)
            updated_asset_folder = get_current_asset_folder()

            if asset_folder != updated_asset_folder :
                print("Old filepath : ", item.filepath)
                basename = item.filepath.split(os.sep)[-1]
                new_path = os.path.join(updated_asset_folder, basename)
                item.filepath = new_path
                print("New filepath : ", new_path)


def create_mesh(name, collection, verts, edges, faces) :
    """Creates a mesh from a list of point, verts, and faces, and adds it to a collection.
    
    cf mesh.from_pydata(...) doc for specifics on verts, edges, and faces.

    Parameters:
    - name (str): The name for the new mesh.
    - collection (bpy.types.Collection): The collection to which the new mesh will be added.
    - verts (list): List of vertices.
    - edges (list): List of edges.
    - faces (list): List of faces.

    Returns:
    - obj (bpy.types.Object): The created Blender object encapsulating the mesh."""

    mesh = bpy.data.meshes.new(name)  # add the new mesh
    obj = bpy.data.objects.new(mesh.name, mesh)
    collection.objects.link(obj)
    bpy.context.view_layer.objects.active = obj
    mesh.from_pydata(verts, edges, faces)
    return obj


def add_image_texture(image_path, object) :
    """Creates a custom material and adds it to a plane for it to display an image with alpha.
    
    - image_path (str): Path of the texture image.
    - object (bpy.types.Object): Plane on which to add the texture."""

    material = bpy.data.materials.new(name=object.name+"_material")

    if object.data.materials:
        object.data.materials[0] = material
    else:
        object.data.materials.append(material)

    material.use_nodes = True
    node_tree = material.node_tree
    nodes = node_tree.nodes

    if "Principled BSDF" in nodes.keys() :
        nodes.remove(nodes["Principled BSDF"])

    image_texture = nodes.new("ShaderNodeTexImage")
    transp_bsdf = nodes.new("ShaderNodeBsdfTransparent")
    mix_shader = nodes.new("ShaderNodeMixShader")
    output = nodes["Material Output"]

    image_texture.interpolation = 'Closest'
    image_texture.image = bpy.data.images.load(image_path)
    image_texture.image.alpha_mode = 'STRAIGHT'

    node_tree.links.new(image_texture.outputs['Color'], mix_shader.inputs[2])
    node_tree.links.new(transp_bsdf.outputs['BSDF'], mix_shader.inputs[1])
    node_tree.links.new(image_texture.outputs['Alpha'], mix_shader.inputs[0])
    node_tree.links.new(mix_shader.outputs['Shader'], output.inputs['Surface'])

    material.blend_method = 'HASHED'


def subdivide_plane(plane_object) :
    """Subdivides the mesh to make subsequent UV_project more precise.

    Parameters:
    - plane_object (bpy.types.Object): The Blender object representing the plane."""

    mesh = plane_object.data
    bm = bmesh.new()
    bm.from_mesh(mesh)

    bmesh.ops.subdivide_edges(bm, edges=bm.edges, cuts=3, use_grid_fill=True)

    bm.to_mesh(mesh)
    mesh.update()


def project_uv_from_view(plane_object) :
    """UV project from the current view.

    Parameters:
    - plane_object (bpy.types.Object): The Blender object representing the plane."""

    window = bpy.context.window
    area  = [area for area in window.screen.areas if area.type == 'VIEW_3D'][0]
    region = [region for region in area.regions if region.type == "WINDOW"][0]

    with bpy.context.temp_override(area=area, region=region):
        bpy.context.view_layer.objects.active = plane_object
        mode_manager.switch_mode("EDIT")

        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.uv.project_from_view(camera_bounds=True, correct_aspect=False, scale_to_bounds=False)

        mode_manager.switch_mode("OBJECT")


def get_collection(name, parent) :
    """Retrieves a collection with a specified name from the parent collection.

    Parameters:
    - name (str): The name of the target collection.
    - parent (bpy.types.Collection): The parent collection."""

    children = parent.children_recursive

    for child in children :
        if child.name == name :
            return child


def create_collection(name, parent) :
    """Creates a new collection with a specified name and adds it to a parent collection.

    Parameters:
    - name (str): The name of the newly created collection.
    - parent (bpy.types.Collection): The parent collection.

    Returns:
    - collection (bpy.types.Collection): The newly created collection."""

    assert name not in bpy.data.collections.keys(), "Texture Planes : tried to create %s collection, but a similarly named collection already exists."

    collection = bpy.data.collections.new(name=name)
    parent.children.link(collection)

    return collection


def create_collection_path(path, scene) :
    """Creates a collection path in the scene hierarchy based on a specified path string.

    Parameters:
    - path (str): The path string for creating the collection hierarchy.
    - scene (bpy.types.Scene): The Blender scene.

    Returns:
    - parent (bpy.types.Collection): The final parent collection in the created hierarchy."""

    scene_collection = scene.collection

    parent = scene_collection

    for name in path.split("/") : 
        col = get_collection(name, parent)

        if col is None :
            col = create_collection(name, parent)

        else :
            l_col = get_layer_collection(col)
            l_col.exclude = False
        
        parent = col
    
    return parent


def set_collection_color(name, color) :
    """Sets the color tag of a collection.

    Parameters:
    - name (str): The name of the target collection.
    - color (str): The color tag to be set."""

    col = bpy.data.collections[name]
    col.color_tag = color


def build_collection(scene, path, color) :

    join = lambda L : "/".join(L)
    col_path = path.split("/")

    if scene.name != "SCN_BUILD" :
        shot_code = scene.name.replace("SCN_", "")
        col_path = [core.scene_split.get_name(p, shot_code) for p in col_path]

    print("Creating collection path : ", join(col_path))

    col = create_collection_path(join(col_path), scene)

    for collection_name in col_path :
        set_collection_color(collection_name, color)

    return col


def buildpaint_collection(scene, object_name="") :
    """Fetches the COL_BG_BUILD_PAINT collection from hierarchy. If it doesn't 
    exists, creates it at COL-GP/COL_BG_BUILD/COL_BG_BUILD_PAINT path, 
    creating any missing collection in the process.

    Parameters:
    - scene (bpy.types.Scene): The Blender scene.

    Returns:
    - col (bpy.types.Collection): The fetched or created collection."""

    if "PROP" in object_name :   
        print("Routing to PROPS")
        path = "COL-GP/COL_PROPS-BG_BUILD/COL_PROPS-BG_BUILD_PAINT"
        color = "COLOR_04" 

    else :
        print("Routing to BUILDPAINT")
        path = "COL-GP/COL_BG_BUILD/COL_BG_BUILD_PAINT"
        color = "COLOR_03"

    return build_collection(scene, path, color)


def create_texture_plane(gp_object, cam) :
    """Creates a mesh plane to replace a specified Grease Pencil object.
    Corners of the plane are projections of corners of the camera onto the gp_object depth plane.
    
    - gp_object (bpy.types.GreasePencil): Grease Pencil object to replace.
    - cam (bpy.types.Camera): Export camera to use as the basis for the corners of the plane.
    
    Returns:
    - obj (bpy.types.Object): Blender object encapsulating the mesh."""

    build_scene = core.scene_shift.get_build_scene()
    core.switch_scene(build_scene)
    gp_object = core.scene_shift.get_object_alias(gp_object, build_scene)

    col = buildpaint_collection(build_scene, gp_object.name)
    resolution = (build_scene.render.resolution_x, build_scene.render.resolution_y)
    
    corners = get_camera_bounds(gp_object.location, cam, resolution)[1:]
    edges = []
    faces = [[3,2,0,1]]

    new_name = gp_object.name.replace("CLEAN", "PAINT")
    if "PAINT" not in new_name :
        new_name += "_PAINT"
    
    obj = create_mesh(new_name, col, corners, edges, faces)

    obj.data.uv_layers.new(name='UV')
    subdivide_plane(obj)
    project_uv_from_view(obj)
    
    return obj


def link_plane_to_scene(plane, gp_object, scene) :
    """Links a plane to a scene, replacing a specified Grease Pencil object.
    
    - plane (bpy.types.Object): Blender object encapsulating the mesh.
    - gp_object (bpy.types.GreasePencil): Grease Pencil object to replace.
    - scene (bpy.types.Scene): Target scene."""

    
    build_scene = core.scene_shift.get_build_scene()

    core.switch_scene(scene)

    build_gp = core.scene_shift.get_object_alias(gp_object, build_scene)
    shot_gp = core.scene_shift.get_object_alias(gp_object, scene)
    
    if shot_gp is not None :
        print("Hiding %s." % shot_gp.name)
        shot_gp.hide_set(True)
        shot_gp.hide_render = True

    col = buildpaint_collection(scene, shot_gp.name)
    shot_code = scene.name.replace("SCN_", "")

    object_name = gp_object.name
    object_name = object_name.replace("GP_", "")
    object_name = object_name.replace("BG_", "")
    new_name = "GEO_BG_%s_PAINT_%s" % (shot_code, object_name)

    obj = bpy.data.objects.new(new_name, plane.data)
    col.objects.link(obj)

    WP0 = np.array(build_gp.matrix_world)
    WP1 = np.array(shot_gp.matrix_world)
    W0 = np.array(plane.matrix_world)

    W1 = WP1 @ np.linalg.inv(WP0) @ W0
    W1 = Matrix(W1.tolist())

    obj.matrix_world = W1


def import_image_assets() :
    """Cycles through every visible Grease Pencil object, and checks 
    if a Photoshop-generated texture is available. If so, it creates 
    a textured plane with the PS texture, and hides the original 
    GP object."""
    
    # scene = core.scene_shift.get_build_scene()
    objects = get_all_visible_GP_Objects()
    cam = core.export_camera.get_export_camera()

    assert cam is not None, "Please make sure Export_Camera exists in SCN_BUILD."

    asset_folder = get_current_asset_folder()

    print('Creating texture planes from : ', asset_folder)
    print("Folder exists ? ", os.path.exists(asset_folder))

    for o in objects :
        texture_path = "".join([asset_folder, "\\", o.name, ".png"])
        
        if os.path.exists(texture_path) :
            print("Creating texture plane for %s."%(o.name))
            plane = create_texture_plane(o, cam)
            add_image_texture(texture_path, plane)
            o.hide_set(True)

            print("Texture plane location after creation : ", plane.location)

            for scene in core.scene_shift.get_scenes_containing_object(o) :
                if scene.name != "SCN_BUILD" :
                    print("Linking %s to %s" % (plane.name, scene.name))
                    link_plane_to_scene(plane, o, scene)
        
        else :
            print("No texture found for %s at %s"%(o.name, texture_path))
    
    collections_to_disable = ["COL_BG_BUILD_ROUGH", "COL_BG_BUILD_CLEAN", "COL_PROPS-BG_BUILD_ROUGH", "COL_PROPS-BG_BUILD_CLEAN"]
    for col_name in collections_to_disable : 
        core.scene_shift.disable_collection_everywhere(col_name)


def get_available_asset_packs() :
    """Scans folder above the one containing the .blend file, searching 
    for subfolders containing '-assets' in their name. Then returns 
    a list of those matching folders absolute path."""

    global last_asset_update_time, last_asset_list

    # Return cached asset list if update is too frequent 
    # to avoid taking up too much ressource

    if last_asset_update_time is not None :
        elasped_time = time.time() - last_asset_update_time
        if elasped_time < 15.0 :
            return last_asset_list
        
    split_path = lambda path : os.path.normpath(path).split(os.sep)
    join_path = lambda path_list : os.sep.join(path_list)

    try :
        current_folder = split_path(get_export_path())

        folders_to_scan = [
            join_path(current_folder),
            join_path(current_folder[0:-2] + ["PAINT_BG"]),
            join_path(current_folder[0:-2] + ["LAYOUT_BG"]),
            # join_path(current_folder[0:-2] + ["02_PAINT"]),
        ]
        
        folders_to_scan = [path for path in folders_to_scan if os.path.exists(path)]

        content = []
        for path in folders_to_scan :
            content += [x[0] for x in os.walk(path) if "-assets" in x[0]]
    
        last_asset_update_time = time.time()
        last_asset_list = content

        return content
    
    except IndexError as e :
        return []