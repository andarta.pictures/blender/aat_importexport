# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np

from .. import core

from ..common.scene_utils import get_object_alias
from ..common.gp_utils import get_layer_collection



def get_camera_original_scene(cam) :
    matching_scenes = []
    for scene in bpy.data.scenes :
        for o in scene.objects :
            if o.data is not None and type(o.data).__name__ == "Camera" :
                if o.name == cam.name :
                    matching_scenes.append(scene)

    print(f"Matching scenes for {cam.name} : {[s.name for s in matching_scenes]}")

    if len(matching_scenes) == 1 and matching_scenes[0].name == "SCN_BUILD" :
        return matching_scenes[0]

    matching_scenes = [scene for scene in matching_scenes if scene.name != "SCN_BUILD"]
    assert len(matching_scenes) > 0, "Projection : No scene was found for %s." % cam.name
    assert len(matching_scenes) == 1, "Projection : %s was found in multiple scenes (not counting SCN_BUILD). Make sure it is present only in one." % cam.name

    return matching_scenes[0]


def get_scene(name) :
    assert name in bpy.data.scenes.keys(), "Projection : no %s was found in the project." % name
    for col in bpy.data.scenes :
        if col.name == name :
            return col

def get_build_scene() :
    return get_scene("SCN_BUILD")


def get_collection_scenes(collection) :
    scenes_containing_collection = []

    for scene in bpy.data.scenes :
        collection_names = [col.name for col in scene.collection.children_recursive]
        if collection.name in collection_names :
            scenes_containing_collection.append(scene)

    return scenes_containing_collection

def in_scene(obj, scene) :
    if obj.data is None :
        return obj.name in [o.name for o in scene.objects]
    
    return obj.data.name in [o.data.name for o in scene.objects if o.data is not None]


def get_scenes_containing_object(obj) :
    matching_scenes = []

    for scene in bpy.data.scenes :
        if in_scene(obj, scene) : 
            matching_scenes.append(scene)

    return matching_scenes


def data_names(object_list) :
    return [o.data.name for o in object_list]


def get_scene_gp_objects(scene) :
    window = bpy.context.window
    previous_scene = window.scene

    window.scene = scene
    viewlayer = window.view_layer
    gp_objects = [o for o in viewlayer.objects if isinstance(o.data, bpy.types.GreasePencil)]

    window.scene = previous_scene
    return gp_objects


def get_common_gp_objects(scene1, scene2) :

    scene1_gp_objects = get_scene_gp_objects(scene1)
    scene2_gp_objects = get_scene_gp_objects(scene2)

    common_objects = []
    for o1 in scene1_gp_objects :
        for o2 in scene2_gp_objects :
            if o1.data.name == o2.data.name :
                common_objects.append((o1, o2))

    return common_objects


def get_T_matrix(object, scene1, scene2) :
    
    o1 = get_object_alias(object, scene1)
    assert o1 is not None, "Projection : no corresponding object was found for %s in %s." % (object.name, scene1.name)
    
    o2 = get_object_alias(object, scene2)
    assert o2 is not None, "Projection : no corresponding object was found for %s in %s." % (object.name, scene2.name)
    
    mat1 = np.array(o1.matrix_world)
    mat2 = np.array(o2.matrix_world)
    T = mat2 @ np.linalg.inv(mat1)

    return T


def get_collection_alias(collection_name, scene) :
    shot_code = scene.name.replace("SCN_", "")
    name = core.scene_split.get_name(collection_name, shot_code)
    assert name in bpy.data.collections, f"'{name}' was not found in collections."
    return bpy.data.collections[name]


def disable_collection_everywhere(col_name) :
    for scene in bpy.data.scenes :
        try : 
            col = get_collection_alias(col_name, scene)
            for vl in scene.view_layers :
                laycol = get_layer_collection(col, vl)
                laycol.exclude = True
        except AssertionError :
            print(f"Couldnt find {col_name} alias in {scene.name}")
