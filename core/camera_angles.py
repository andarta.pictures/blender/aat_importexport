# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np

from .projection_clusters import ProjectionCluster


def pretty_angle(angle) :
    angle = tuple([np.rad2deg(v) for v in angle])
    return "(%.1f°<>%.1f°, %.1f°<>%.1f°)" % angle


def angle_to_camera(bounds, camera_position) :
    cam_X, cam_Y, cam_Z = camera_position
    min_X, max_X, min_Z, max_Z, centroid = bounds

    d = abs(centroid[1]-cam_Y)

    alpha1 = np.arctan((min_X-cam_X)/d)
    alpha2 = np.arctan((max_X-cam_X)/d)
    
    beta1 = np.arctan((min_Z-cam_Z)/d)
    beta2 = np.arctan((max_Z-cam_Z)/d)

    return alpha1, alpha2, beta1, beta2


def get_aperture(projection_clusters : ProjectionCluster, cam_position, crop_to_geometry=True) :
    """Calculates camera aperture in regard to a certain camera position.
    
    Params : 
    - projection_clusters : cf core.import_export.projection_clusters
    - cam_position : 3D tuple for world location of the camera
    
    Returns : 
    - alpha1 : angle in radians between Y axis and line between camera and left most object point
    - alpha2 : same for right most point
    - beta1 : same for bottom most point
    - beta2 : same for top most point"""

    cluster = projection_clusters

    if crop_to_geometry :
        cluster = projection_clusters.crop_to_geometry()

    angle = cluster.angle_to_camera(cam_position)
    print("Final angle : ", pretty_angle(angle))
    return angle


def calculate_euler_FOV_angle(FOV_angle, cam_resolution) :
    """Calculates alpha and beta components of the 
    FOV angle, based on aspect ratio of the image. 
    
    - FOV_angle : field of view angle of the camera in 
    radians (often obtained through camera.data.angle) 
    - cam_resolution : (res_x, res_y) resolution of the camera
    
    Returns : 
    - alpha : FOV_angle on X-axis
    - beta : FOV_angle on Z-axis"""

    rx = cam_resolution[0]/2
    ry = cam_resolution[1]/2
    
    if rx > ry : 
        alpha = FOV_angle/2
        beta = np.arctan(np.tan(alpha)*ry/rx)

    else : 
        beta = FOV_angle/2
        alpha = np.arctan(np.tan(beta)*rx/ry)

    beta_p = np.arctan(np.tan(beta)*np.cos(alpha))
    return (alpha, beta_p)