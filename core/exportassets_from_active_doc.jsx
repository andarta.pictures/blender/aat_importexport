//#target photoshop

// Fonction principale
function exportPngFromGroups(doc) {
    if (!app.documents.length) {
        alert("Aucun document ouvert.");
        return;
    }

    //var doc = app.activeDocument;
    var psdName = doc.name.replace(/\.[^\.]+$/, ""); // Nom du PSD sans extension
    var outputFolder = new Folder(doc.path + "/" + psdName + "-assets");

    // Créer le dossier de sortie s'il n'existe pas
    if (!outputFolder.exists) {
        outputFolder.create();
    }

    // Parcourir les groupes de calques du premier niveau et exporter ceux nommés "*.png"
    for (var i = 0; i < doc.layerSets.length; i++) {
        var group = doc.layerSets[i];
        if (group.name.match(/\.png$/)) {
            exportGroupAsPng(group, outputFolder);
        }
    }
    //doc.close(SaveOptions.DONOTSAVECHANGES);
}

// Fonction pour exporter un groupe en PNG
function exportGroupAsPng(group, outputFolder) {
    var doc = app.activeDocument;

    // Dupliquer le document pour isoler le groupe
    var tempDoc = doc.duplicate();
    app.activeDocument = tempDoc; // S'assurer que le document temporaire est actif

    // Masquer tous les calques dans le document temporaire
    for (var i = 0; i < tempDoc.layers.length; i++) {
        tempDoc.layers[i].visible = false;
    }

    // Rendre visible uniquement le groupe ciblé (premier niveau)
    var targetGroup = tempDoc.layerSets.getByName(group.name);
    targetGroup.visible = true;

    // Définir la profondeur de couleur à 32 bits
    //tempDoc.bitsPerChannel = BitsPerChannelType.THIRTYTWO;

    // Définir le fichier de sortie
    //var outputFile = new File(outputFolder + "/" + group.name.replace(/(\.[^\.]+)$/, "-bis$1")); //pour test
    var outputFile = new File(outputFolder + "/" + group.name);

    // Créez un nouvel objet PNGSaveOptions
    var pngOptions = new PNGSaveOptions();
    pngOptions.compression = 9; // Compression maximale (valeur de 0 à 9)
    pngOptions.interlaced = false; // Ne pas utiliser l'entrelacement
    pngOptions.saveProfile = false; // Ne pas enregistrer le profil ICC (si vous voulez l'exclure)
    //pngOptions.method = 'QUICK'

    // Enregistrer l'image en PNG sans boîte de dialogue
    tempDoc.saveAs(outputFile, pngOptions, true, Extension.LOWERCASE);

    // Fermer le document temporaire sans sauvegarder
    tempDoc.close(SaveOptions.DONOTSAVECHANGES);  
}

// Lancer le script
var doc = app.activeDocument
exportPngFromGroups(doc);
