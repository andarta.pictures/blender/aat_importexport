# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import os
import json, re
import numpy as np

from ..common.gp_utils import sort_by_depth

from .. import core


def get_project_name() :
    """Infer project name from file name, and returns it."""

    project_name = bpy.path.basename(bpy.context.blend_data.filepath)
    project_name = project_name.replace(".blend", "")
    return project_name


def get_export_path() :
    """Returns the path where files are exported to Photoshop. 
    For now it is the folder containing the blend file."""

    path = os.path.dirname(bpy.data.filepath)
    if path[-1] != "\\" :
        path += "\\"
    return path


def create_safe_directory(path) :
    """Create a directory in a safe way if it doesn't exist."""

    if not os.path.exists(path) :
        os.mkdir(path)
    

def photoshopify_path(path) :
    if path[1] == ":" :
        path = "/" + path[0].lower() + "/" + path[3:]
    path = path.replace("\\", "/")
    return path


def get_file_list(objects) : 
    """Returns a list of all the exported PNG files for them to be imported in Photoshop."""

    file_list = {}

    for o in objects : 
        layer_list = []

        for l in o.data.layers :
            if not l.hide :
                name = "%s_%s.png" % (o.name, l.info)
                layer_list.append(name)
        
        file_list["%s.png" % o.name] = layer_list

    return file_list


def write_export_data(objects) :
    """Writes a JSON file at export location (defined by get_export_path), 
    containing every necessary information for the Photoshop import script.
    
    - objects : list of renderable Blender objects to be listed in the 
    data and imported in Photoshop"""

    export_directory = get_export_path()
    create_safe_directory(export_directory)

    scene = bpy.context.scene
    project_name = get_project_name()

    export_path = export_directory + project_name + "_export.json"

    resolution = [scene.render.resolution_x, scene.render.resolution_y]

    psd_path = get_export_path() + project_name + "_BG.psd"
    psd_path = photoshopify_path(psd_path)

    image_folder = get_export_path() + "GP_renders\\"
    image_folder = photoshopify_path(image_folder)

    cam_location = scene.camera.location
    cam_rotation = scene.camera.rotation_euler
    cam_angle = scene.camera.data.angle

    objects = sort_by_depth(objects)
    files = get_file_list(objects)
    
    output = [
        "var data = {", 
        "    resolution : %s," % str(resolution),
        "    cam_location : \"%s\"," % str(cam_location),
        "    cam_rotation : \"%s\"," % str(cam_rotation),
        "    cam_angle : %s," % str(cam_angle),
        "    PS_file : '%s'," % psd_path, 
        "    image_folder : '%s'," % image_folder, 
        "    files : %s," % str(files),
        "}",
    ]

    with open(export_path, "w") as outfile :
        outfile.write("\n".join(output))



# def add_property_quotes(str_data) :
#     return re.sub(r'([a-zA-Z_][a-zA-Z0-9_]*)\s*:', r'"\1":', str_data)


def normalize_element(string) :
    return re.sub(r'(^[\s"]*)|([\s,"]*$)', '', string)


def get_JSON_data(path) :

    with open(path, 'r') as infile :
        str_data = infile.read()
    
    str_data = str_data.replace("var data = ", "")
    data = {}

    for line in str_data.split("\n") :
        if ":" in line :
            elements = line.split(":")
            name = normalize_element(elements[0])
            content = normalize_element(":".join(elements[1:]))
            data[name] = content

    return data


def breakdown(content) :
    content = content.replace("<Vector", "")
    content = content.replace("<Euler", "")
    content = re.sub(r"order='[XYZ]*'", '', content)
    content = re.sub(r"[xyz]=", '', content)
    content = re.sub(r"\s*,\s*", ',', content)
    content = re.sub(r'(^[\s()\[\],<>]*)|([\s()\[\],<>]*$)', '', content)
    
    data = content.split(",")
    return data


def get_export_cam_data(path) :
    data = get_JSON_data(path)

    resolution = breakdown(data['resolution'])
    resolution = [int(e) for e in resolution]

    cam_location = breakdown(data['cam_location'])
    cam_location = [float(e) for e in cam_location]

    cam_rotation = breakdown(data['cam_rotation'])
    cam_rotation = [float(e) for e in cam_rotation]

    cam_angle = breakdown(data['cam_angle'])
    cam_angle = float(cam_angle[0])

    print("\nRestoring export camera : ")
    print("- Resolution : %dx%d" % (resolution[0], resolution[1]))
    print("- Location : (%.3f, %.3f, %.3f)" % (cam_location[0], cam_location[1], cam_location[2]))
    print("- Rotation : (%.3f, %.3f, %.3f)" % (cam_rotation[0], cam_rotation[1], cam_rotation[2]))
    print("- Angle : %.1f°" % np.rad2deg(cam_angle))
    return {

        "resolution" : resolution,
        "cam_location" : cam_location,
        "cam_rotation" : cam_rotation,
        "cam_angle" : cam_angle,
    }

    