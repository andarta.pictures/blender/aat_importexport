# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import math
import numpy as np

from ..common.geometry_3D import rotate, rmat, calculate_intersection, print_m
from ..common.empty_display import create_empty, display_angle
from ..common.gp_utils import get_object_depth, get_geo, get_scene_gp_objects
from ..common import log_3D
from ..common.scene_utils import get_object_alias

from .. import core

from . import utils


class Projection :
    """Data structure meant to store a single camera projection."""

    def __init__(self, object_name, frame, camera_name, scene_name, projection):
        """
        Create a data structure for a single camera projection.
        
        - object_name : name of the object
        - frame : frame used to determine the camera position for the projection
        - camera_name : name of the camera
        - scene_name : name of the scene the projection was shot in
        - points : list of 5 3D vectors [center, top_left_corner, top_right_corner, 
        bottom_left_corner, bottom_right_corner]

        """
        self.object_name = object_name
        self.frame = frame
        self.camera_name = camera_name
        self.points = projection
        self.scene_name = scene_name

    def width(self) :
        top_left_corner = np.array(self.points[1])
        top_right_corner = np.array(self.points[2])
        return np.linalg.norm(top_right_corner - top_left_corner)

    def height(self) :
        top_left_corner = np.array(self.points[1])
        bottom_left_corner = np.array(self.points[3])
        return np.linalg.norm(bottom_left_corner - top_left_corner)


class ProjectionCluster : 
    data : list

    def __init__(self) :
        """Creates an empty projection cluster."""
        self.data = []


    def duplicate(self) :
        """Creates a separate copy of the cluster and returns it."""
        cluster_copy = ProjectionCluster()
        cluster_copy.set_data(self.data.copy())
        return cluster_copy


    def append_projection(self, object, frame, camera_name, scene_name, projection) :
        """
        Appends a new projection to the cluster.
        
        - object : name of the object
        - frame : frame used to determine the camera position for the projection
        - camera_name : name of the camera
        - scene_name : name of the scene the projection was shot in
        - projection : list of 5 3D vectors [center, top_left_corner, top_right_corner, 
        bottom_left_corner, bottom_right_corner]
        
        """

        p = Projection(object, frame, camera_name, scene_name, projection)
        self.data.append(p)


    def append_cluster(self, cluster) :
        """Append a cluster to the current one to fuse them together."""
        self.data += cluster.data


    def set_data(self, data) :
        """Swaps the underlying data structure with a new one."""
        self.data = data


    def size(self) :
        """Returns the number of projections stored in the cluster."""
        return len(self.data)


    def filter_by_object(self, object_name) :
        """Returns a new cluster only containing the projections on a specific object."""

        new_data = []

        for projection in self.data :
            if projection.object_name == object_name :
                new_data.append(projection)
        
        new_cluster = ProjectionCluster()
        new_cluster.set_data(new_data)

        return new_cluster


    def filter_by_frame(self, frame) :
        """Returns a new cluster only containing the projections from a specific frame."""

        new_data = []

        for projection in self.data :
            if projection.frame == frame :
                new_data.append(projection)
        
        new_cluster = ProjectionCluster()
        new_cluster.set_data(new_data)

        return new_cluster


    def filter_by_camera(self, camera_name) :
        """Returns a new cluster only containing the projections from a specific camera."""

        new_data = []

        for projection in self.data :
            if projection.camera_name == camera_name :
                new_data.append(projection)
        
        new_cluster = ProjectionCluster()
        new_cluster.set_data(new_data)

        return new_cluster


    def filter_by_scene(self, scene_name) :
        """Returns a new cluster only containing the projections from a specific scene."""

        new_data = []

        for projection in self.data :
            if projection.scene_name == scene_name :
                new_data.append(projection)
        
        new_cluster = ProjectionCluster()
        new_cluster.set_data(new_data)

        return new_cluster


    def get_projections(self, concatenated=False) :
        """
        Returns a numpy array containing the projection data.
        
        - concatenated : if True, the array is a Nx3 list of all the projected points, if not it 
        is a Nx5x3 list of groups of 5 projection vectors (4 corners and the center)
        """
        
        projections = []

        for p in self.data : 
            if concatenated :
                projections += p.points
            else :
                projections.append(p.points)

        return np.array(projections)


    def get_geo_bounds(self, scene=None) :
        """Returns a dict containing, for each object_name in the cluster, a list comprised of 
        (min_X, max_X, min_Z, max_Z, centroid), 2D boundaries of the object."""

        if scene is None :
            scene = bpy.context.scene

        geo_bounds = {}
        
        for o_name in self.get_object_list() :
            obj = bpy.data.objects[o_name]
            obj = core.scene_shift.get_object_alias(obj, scene)
            assert obj is not None, "Projection : %s was not found in %s" % (obj.name, scene.name)
            
            geo = np.array(get_geo(obj, stroke_length_threshold=4))
            bounds = utils.calculate_max_bounds(geo)
            geo_bounds[o_name] = bounds
        
        return geo_bounds
    

    def get_bounds(self) :
        """Returns a list comprised of (min_X, max_X, min_Z, max_Z, centroid), 
        2D boundaries all the projection points."""

        projections = self.get_projections(concatenated=True)
        return utils.calculate_max_bounds(projections)
    

    def get_object_list(self) :
        """Returns a list of every unique object in the projection data."""
        return list(set([p.object_name for p in self.data]))
    

    def get_frame_list(self) :
        """Get a list of every unique frame in the projection data."""
        return list(set([p.frame for p in self.data]))
    

    def get_camera_list(self) :
        """Returns a list of every unique camera in the projection data."""
        return list(set([p.camera_name for p in self.data]))
    

    def get_scene_list(self) :
        """Returns a list of every unique scene in the projection data."""
        return list(set([p.scene_name for p in self.data]))


    def crop_to_geometry(self, scene=None) :
        """Returns a new cluster, containing the same projections as this one, 
        but cropped to the geometry boundaries."""

        if scene is None :
            scene = bpy.context.scene

        geo_bounds = self.get_geo_bounds(scene)
        cluster = ProjectionCluster()

        orthogonalized_cluster = self.orthogonalize()

        for projection in orthogonalized_cluster.data :
            o_name = projection.object_name
            geo_min_X, geo_max_X, geo_min_Z, geo_max_Z, geo_centroid = geo_bounds[o_name]

            points = projection.points
            new_points = []

            for p in points :
                x,y,z = p
                x = min(x, geo_max_X)
                x = max(x, geo_min_X)
                z = min(z, geo_max_Z)
                z = max(z, geo_min_Z)
                new_points.append((x,y,z))
            
            x_vals = [p[0] for p in new_points]
            z_vals = [p[2] for p in new_points]
            amplitude = lambda l : max(l) - min(l)

            if amplitude(x_vals) > 0 and amplitude(z_vals) > 0 :
                cluster.append_projection(projection.object_name, projection.frame, projection.camera_name, projection.scene_name, new_points)
        
        return cluster


    def display(self, size=1, color=None) :
        """
        Using the Log3D library in the commons, displays every projection contained in the cluster.
        
        - size : size of the line
        - color : color of the line ("GREEN", "ORANGE", "RED", "PURPLE", "YELLOW", "BLUE", "PINK")

        """

        if color is None :
            color = log_3D.get_random_color()
        
        # print("Displaying cluster of size %d with color %s" % (self.size(), color))
        
        for projection in self.data :
            polyline = [list(projection.points[i]) for i in [1,2,4,3,1,4,3,2]]
            log_3D.line(polyline, color_name=color, size=size)
    
    
    def display_bounds(self) :
        """Displays one rectangle containing all of the projections."""

        min_X, max_X, min_Z, max_Z, centroid = self.get_bounds()
        depth = centroid[1]
        
        polyline = [
            (min_X, depth, min_Z),
            (max_X, depth, min_Z),
            (max_X, depth, max_Z),
            (min_X, depth, max_Z),
            (min_X, depth, min_Z),
        ]
        
        log_3D.line(polyline, size=1)


    def angle_to_camera(self, camera_location) :
        """
        Calculates camera aperture corresponding to the projection cluster in regard to a certain camera position.
    
        Params : 
        - camera_location : 3D tuple for world location of the camera
        
        Returns : 
        - alpha1 : angle between Y axis and line between camera and left most projection point
        - alpha2 : same for right most point
        - beta1 : same for bottom most point
        - beta : same for top most point
        
        """

        apertures = []

        for o_name in self.get_object_list() :
            object_cluster = self.filter_by_object(o_name)
            bounds = object_cluster.get_bounds()
            aperture = core.camera_angles.angle_to_camera(bounds, camera_location)

            apertures.append(aperture)

        apertures = np.array(apertures)

        alpha1 = np.min(apertures[:,0])
        alpha2 = np.max(apertures[:,1])
        beta1 = np.min(apertures[:,2])
        beta2 = np.max(apertures[:,3])

        # color_name = log_3D.get_random_color()
        # dv = np.array([0,100,0])
        # log_3D.angle(alpha1, 0.0, camera_location, color_name=color_name, dv=dv)
        # log_3D.angle(alpha2, 0.0, camera_location, color_name=color_name, dv=dv)
        # log_3D.angle(0.0, beta1, camera_location, color_name=color_name, dv=dv)
        # log_3D.angle(0.0, beta2, camera_location, color_name=color_name, dv=dv)

        return alpha1, alpha2, beta1, beta2
    

    def cast_into_scene(self, scene) :
        """Returns a new cluster, containing the same projections as this one, 
        but cast into another scene space."""

        cluster = ProjectionCluster()

        scene_list = self.get_scene_list()

        for scene_name in scene_list :
            scene_cluster = self.filter_by_scene(scene_name)
            og_scene = core.scene_shift.get_scene(scene_name)

            for o_name in scene_cluster.get_object_list() :
                object_cluster = scene_cluster.filter_by_object(o_name)
                o = bpy.data.objects[o_name]

                t_matrix = core.scene_shift.get_T_matrix(o, og_scene, scene)

                if not (t_matrix == np.identity(4)).all() :
                    print_m("\nCasting projections %s>%s\n%s T_matrix : "%(og_scene.name, scene.name, o_name), t_matrix)
                
                else : 
                    print("\nCasting projections %s>%s\n%s : no displacement."%(og_scene.name, scene.name, o_name))

                for p in object_cluster.data :
                    P = np.array(p.points)
                    P = np.append(P, np.ones((5,1)), axis=1)
                    P = P.transpose()

                    P_t = t_matrix @ P

                    P_t = P_t[0:3, :]
                    P_t = P_t.transpose()
                    new_points = P_t.tolist()

                    cluster.append_projection(p.object_name, p.frame, p.camera_name, p.scene_name, new_points)

        return cluster
    
    def orthogonalize(self) :
        """Returns a new cluster, containing the tightest bounds containing the 
        projections while being orthogonal with the space XZ axes."""

        cluster = ProjectionCluster()

        for projection in self.data :
            points = np.array(projection.points)
            min_X, max_X, min_Z, max_Z, centroid = utils.calculate_max_bounds(points)
            mean_Y = centroid[1]

            # Projection points : center, top_left_corner, top_right_corner, 
            # bottom_left_corner, bottom_right_corner

            new_points = [
                centroid, 
                (min_X, mean_Y, max_Z),
                (max_X, mean_Y, max_Z),
                (min_X, mean_Y, min_Z),
                (max_X, mean_Y, min_Z),
            ]

            cluster.append_projection(projection.object_name, projection.frame, projection.camera_name, projection.scene_name, new_points)
        
        return cluster



def get_camera_bounds(obj_location, camera, resolution=None) :
    """ Calculates projected camera bounds on specified object.
    - obj_location : location of the object (mathutils.Vector)
    - camera : a camera object (Object with Camera data)

    Returns list of 5 3D positionnal vectors corresponding to the field 
    of view of the camera projected onto the GP_object: 
    [center, top_left_corner, top_right_corner, bottom_left_corner, bottom_right_corner]"""

    cam_location = camera.location
    cam_rotation = camera.rotation_euler

    obj_loc = np.array(obj_location)
    cam_loc = np.array(cam_location)

    positions = []

    scene = core.scene_shift.get_build_scene()
    if resolution is None :
        resolution = (scene.original_resolution[0], scene.original_resolution[1])

    l = np.array([0,1,0])

    # Calculate euler FOV angle. The applied order of the rotation 
    # is important, beta first on X plane, then alpha on Z plane.
    # e.g. rmat(alpha, "Z") @ ( rmat(-beta, "X") @ l )
    alpha, beta = core.camera_angles.calculate_euler_FOV_angle(camera.data.angle, resolution)

    # Calculate direction vector for each angle of the camera
    directions = [
        l,
        rmat(-alpha, "Z") @ rmat(beta, "X") @ l,
        rmat(alpha, "Z") @ rmat(beta, "X") @ l,
        rmat(-alpha, "Z") @ rmat(-beta, "X") @ l,
        rmat(alpha, "Z") @ rmat(-beta, "X") @ l,
    ]

    # Rotate each direction vector with camera rotation
    rotation_adjusted = np.array(cam_rotation) - np.array([math.pi/2, 0, 0])
    R = rmat(rotation_adjusted[2], 'Z') @ rmat(rotation_adjusted[1], 'Y') @ rmat(rotation_adjusted[0], 'X')
    for i in range(0, len(directions)) :
        directions[i] = R @ directions[i]

    # Calculate intersections between GP_object plane and each corner direction.
    for d in directions :
        intersect = calculate_intersection(obj_loc, np.array([0,1,0]), cam_loc, d)
        positions.append(intersect)

    return positions


def calculate_projection_clusters(cam, gp_objects) :
    """For each object, returns a cluster of points containing 
    the projection on the object surface of the camera bounds.
    
    - cam : bpy.types.Camera used for the scene
    - gp_objects : list of Grease Pencil objects to project the camera bounds on
    
    Returns a dictionnary where each entry is a list of 5-tuplets of projected bounds 
    
    e.g : cluster['Object.001'] = [[center, top_left_corner, top_right_corner, 
    bottom_left_corner, bottom_right_corner] at frame 1, same at frame 2, ...]"""

    scene = bpy.context.scene
    og_frame = scene.frame_current

    projection_cluster = ProjectionCluster()
    depth_map = {}

    for gp in gp_objects :
        depth_map[gp.name] = get_object_depth(gp)

    print("Calculating projection clusters for %s>%s from frames [%d>>%d] for objects" % (scene.name, cam.name, scene.frame_start, scene.frame_end), gp_objects)

    for i in range(scene.frame_start, scene.frame_end+1) :
        scene.frame_set(i)
        for gp in gp_objects :
            depth = depth_map[gp.name]
            if depth > cam.location.y :
                projection = get_camera_bounds(gp.location, cam)
                projection_cluster.append_projection(gp.name, i, cam.name, scene.name, projection)
            else :
                print("%s excluded from %s projection : depth = %.3f, cam depth = %.3f" % (gp.name, cam.name, depth, cam.location.y))

    scene.frame_set(og_frame)

    return projection_cluster


def get_current_combined_cluster() :
    """Get a projection cluster of every exported camera (cf core.get_exported_cams) 
    onto every renderable objects (see core.render.get_renderable_objects)"""

    cams = core.get_exported_cams()
    build_scene = core.scene_shift.get_build_scene()

    renderable_build_objects = core.render.get_project_render_list()
    print(f"Renderable objects in '{build_scene.name}' : {[o.name for o in renderable_build_objects]}")
    assert len(renderable_build_objects), "No renderable objects were found for projection calculations, please make sure all objects are renderable and that all of their parent collections are activated."

    combined_cluster = core.projection.ProjectionCluster()

    for c in cams :
        shot_scene = core.scene_shift.get_camera_original_scene(c)
        core.switch_scene(shot_scene)
        
        scene_gp_objects = [get_object_alias(o, shot_scene) for o in renderable_build_objects]
        scene_gp_objects = [o for o in scene_gp_objects if o is not None]
        print(f"'{shot_scene.name}' objects : ", [o.name for o in scene_gp_objects])

        cam_cluster = core.projection.calculate_projection_clusters(c, scene_gp_objects)
        combined_cluster.append_cluster(cam_cluster)

    core.switch_scene(build_scene)
    return combined_cluster