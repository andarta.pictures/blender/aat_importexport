# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import numpy as np

from .. import core

def calculate_max_bounds(cluster) :
    """Returns minimum and maximum values in X and Z axis.
    
    - cluster : list of 3D vectors
    
    Returns (min_X, max_X, min_Z, max_Z, centroid)"""

    X_positions = cluster[:, 0]
    Z_positions = cluster[:, 2]

    min_X = np.min(X_positions)
    max_X = np.max(X_positions)
    min_Z = np.min(Z_positions)
    max_Z = np.max(Z_positions)

    centroid = np.mean(cluster, axis=0)

    return min_X, max_X, min_Z, max_Z, centroid

