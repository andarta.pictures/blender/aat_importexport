'''Based on NijiGPen https://github.com/chsh2/nijiGPen/ License: GPL-3.0-or-later'''
import math
import bpy
import numpy as np
from mathutils import *
from .import_utils import ensure_lib

dependency = ensure_lib('scipy')
if dependency:
    from scipy.sparse import csr_matrix, csgraph
else:
    print("NijiGPen: Failed to import scipy. Some functions may not work.")
    csr_matrix, csgraph = None, None


SCALE_CONSTANT = 8192

class SmartFillSolver:
    tr_map: dict
    graph: csr_matrix
    solid_edges: set                # Edges from original strokes are treated as "walls" that prevent nodes from connecting to each other
    labels: np.ndarray              # Labels of each triangle (node) to mark different colors
    tr_center_kdt: kdtree.KDTree    # KDTree based on the center coordinate of each triangle
    link_ends_map: dict             # Mapping a triangle pair to a vertex pair
        
    def build_graph(self, tr_map):
        """
        Build a graph data structure in the following way:
            triangle -> node, edge shared by two triangles -> node link, edge length -> link weight
        Also, label triangles with bounary edges as transparent
        """

        def e_key(e):
            return (min(e[0],e[1]), max(e[0],e[1]))
        def e_weight(e):
            src = tr_map['vertices'][e[0]]
            dst = tr_map['vertices'][e[1]]
            res = np.sqrt((dst[0]-src[0])**2 + (dst[1]-src[1])**2)
            return max(res, 1e-9)
    
        num_nodes = len(tr_map['triangles'])
        edge_map = {}           # Triangles that shares an edge
        self.solid_edges = set()     
        self.tr_center_kdt = kdtree.KDTree(num_nodes)
        self.link_ends_map = {}
        
        # Put edge and triangle information into maps
        for i,edge in enumerate(tr_map['segments']):
            if len(tr_map['orig_edges'][i]) > 0:
                self.solid_edges.add(e_key(edge))
        for i,tri in enumerate(tr_map['triangles']):
            for edge in (e_key((tri[0],tri[1])), e_key((tri[0],tri[2])), e_key((tri[2],tri[1]))):
                if edge not in edge_map:
                    edge_map[edge] = []
                edge_map[edge].append(i)
            self.tr_center_kdt.insert( ( (tr_map['vertices'][tri[0]][0]+tr_map['vertices'][tri[1]][0]+tr_map['vertices'][tri[2]][0])/3.0
                                      ,  (tr_map['vertices'][tri[0]][1]+tr_map['vertices'][tri[1]][1]+tr_map['vertices'][tri[2]][1])/3.0, 0)
                                      , i)
        self.tr_center_kdt.balance()
        
        # Prepare data for sparse graph (bidirectional)
        row, col, data = [], [], []
        for edge in edge_map:
            if len(edge_map[edge]) > 1:
                self.link_ends_map[e_key((edge_map[edge][0], edge_map[edge][1]))] = edge
                row += [edge_map[edge][0], edge_map[edge][1]]
                col += [edge_map[edge][1], edge_map[edge][0]]
                data += ([e_weight(edge)] * 2) if edge not in self.solid_edges else [1e-9, 1e-9]
        self.tr_map = tr_map
        self.graph = csr_matrix((data, (row, col)), shape=(num_nodes, num_nodes))
        
        # Label boundary nodes as zeros
        self.labels = -np.ones(num_nodes).astype('int32')
        for edge in edge_map:
            if len(edge_map[edge]) == 1:
                self.labels[edge_map[edge][0]] = 0
                
    def set_labels_from_points(self, points_co, points_label):
        """
        Label each node(triangle) according to input points inside it
        """
        import pyclipper
        def point_in_triangle(point_co, tr_idx):
            poly = [self.tr_map['vertices'][i] for i in self.tr_map['triangles'][tr_idx]]
            return (pyclipper.PointInPolygon(point_co, poly) != 0)
        
        # Approximate search using KDTree
        if len(points_co)>0:
            _, current_tr, _ = self.tr_center_kdt.find((points_co[0][0], points_co[0][1], 0))
        # Exact serach using BFS
        for i, co in enumerate(points_co):
            search_queue = [current_tr]
            pointer = 0
            searched = set()
            while pointer < len(search_queue):
                current_tr = search_queue[pointer]
                # To speed up, just label the triangle with the first point found inside it
                if point_in_triangle(co, current_tr):
                    if self.labels[current_tr] < 0:
                        self.labels[current_tr] = points_label[i]
                    break
                # Move to the next triangle
                searched.add(current_tr)
                for next_tr in self.graph.getrow(current_tr).nonzero()[1]:
                    if next_tr not in searched:
                        search_queue.append(next_tr)
                pointer += 1

    def propagate_labels(self):
        """
        Fill unlabelled triangles with known labels, using max-flow min-cut
        """
        num_nodes = self.graph.shape[0]
        graph_links = self.graph.nonzero()
        max_capacity = 65535     # A constant larger than any link weight
        source = num_nodes
        sink = num_nodes + 1
        
        # Perform max-flow once for every color
        for current_label in range(max(self.labels), 0, -1):
            # Build a new graph excluding the labels that have been processed
            row, col, data = [], [], []
            for i in range(len(graph_links[0])):
                n0, n1 = graph_links[0][i], graph_links[1][i]
                if self.labels[n0] <= current_label and self.labels[n1] <= current_label:
                    row.append(n0)
                    col.append(n1)
                    data.append(int(self.graph[n0,n1]))
            # Add two more nodes, source (-2) and sink (-1)
            for i in range(num_nodes):
                if self.labels[i] == current_label:
                    row += [i, source]
                    col += [source, i]
                    data += [max_capacity, max_capacity]
                elif self.labels[i] > -1 and self.labels[i] < current_label:
                    row += [i, sink]
                    col += [sink, i]
                    data += [max_capacity, max_capacity]
                
            # Set labels based on the residual graph of max-flow
            tmp_graph = csr_matrix((data, (row, col)), shape=(num_nodes+2, num_nodes+2))
            res = csgraph.maximum_flow(tmp_graph, source, sink)
            if hasattr(res, 'flow'):    # depending on Scipy versions
                cut_graph = tmp_graph - res.flow
            else:
                cut_graph = tmp_graph - res.residual
            source_nodes = csgraph.depth_first_order(cut_graph, source)[0]
            for node in source_nodes:
                if node < len(self.labels) and self.labels[node] == -1:
                    self.labels[node] = current_label

    def complete_labels(self):
        """
        Assign labels of all unlabelled (-1) nodes according to their neighbors
        """
        num_nodes = len(self.tr_map['triangles'])
        graph_links = self.graph.nonzero()
        
        # Get all connected unlabelled components
        row, col = [], []
        for i in range(len(graph_links[0])):
            n0, n1 = graph_links[0][i], graph_links[1][i] 
            if self.labels[n0] == -1 and self.labels[n1] == -1:
                row.append(n0)
                col.append(n1)
        unlabelled_graph = csr_matrix((np.ones(len(row)), (row, col)), shape=(num_nodes, num_nodes))
        cnum, components = csgraph.connected_components(unlabelled_graph, directed=False)
        
        # Check neighbor labels of these unlabelled components
        neighbor_label_weights = np.zeros((cnum,max(self.labels)+1))
        for i in range(len(graph_links[0])):
            n0, n1 = graph_links[0][i], graph_links[1][i] 
            if self.labels[n0] > -1 and self.labels[n1] == -1:
                neighbor_label_weights[components[n1]][self.labels[n0]] += self.graph[n0,n1]
        components_new_label = np.argmax(neighbor_label_weights, axis=1)
        
        # Assign a new label to each component
        for n,c in enumerate(components):
            if self.labels[n] == -1:
                self.labels[n] = components_new_label[c]
            
    def get_contours(self): 
        """
        Get the outer contour of each connected component of the graph
        """
        import pyclipper
        clipper = pyclipper.PyclipperOffset()
        def e_key(e):
            return (min(e[0],e[1]), max(e[0],e[1]))

        # Find links connecting two nodes with different labels
        # Remove them from the original graph
        num_nodes = len(self.tr_map['triangles'])
        contour_links = set()
        graph_links = self.graph.nonzero()
        row, col = [], []
        for i in range(len(graph_links[0])):
            n0, n1 = graph_links[0][i], graph_links[1][i]
            if self.labels[n0] != self.labels[n1]:
                contour_links.add(e_key((n0,n1)))
            else:
                row.append(n0)
                col.append(n1)
        segmented_graph = csr_matrix((np.ones(len(row)), (row, col)), (num_nodes,num_nodes))
        cnum, components = csgraph.connected_components(segmented_graph, directed=False)
        
        # Identify the color label of each segmented component
        components_label = np.zeros(cnum).astype('int32')
        for n,c in enumerate(components):
            components_label[c] = self.labels[n]
        # Categorize contour links based on the two components they belong to
        contour_vertices = []
        for _ in range(cnum):
            contour_vertices.append({'row':[], 'col':[]})
        for link in contour_links:
            n0, n1 = link[0], link[1]
            for c in (components[n0], components[n1]):
                if components_label[c] > 0:
                    v0, v1 = self.link_ends_map[e_key((n0,n1))]
                    contour_vertices[c]['row'].append(v0)
                    contour_vertices[c]['col'].append(v1)
                    
        # Process the contour of each segmented component
        contours_co = []
        num_verts = len(self.tr_map['vertices'])
        for c in range(cnum):
            contours_co.append([])
            if components_label[c] < 1:
                continue
            
            contour_graph = csr_matrix((np.ones(len(contour_vertices[c]['row'])),
                                        (contour_vertices[c]['row'], contour_vertices[c]['col'])),
                                        shape = (num_verts,num_verts))
            sub_cnum, sub_components = csgraph.connected_components(contour_graph, directed=False)
            component_map = {}
            for v,i in enumerate(sub_components):
                if i not in component_map:
                    component_map[i] = [0, v]
                component_map[i][0] += 1
            
            # Each connected subgraph should have a ring topology. Therefore, the contour can be found through DFS.
            contour_candidates = []
            for i in range(sub_cnum):
                if component_map[i][0] > 2:      # A contour should have at least 3 points
                    vseq = csgraph.depth_first_order(contour_graph, component_map[i][1], 
                                              directed=False, return_predecessors=False)
                    co_seq = [self.tr_map['vertices'][v] for v in vseq]
                    # Eliminate self overlaps
                    clipper.Clear()
                    clipper.AddPath(co_seq, join_type = pyclipper.JT_ROUND, end_type = pyclipper.ET_CLOSEDPOLYGON)
                    contour_candidates += clipper.Execute(0)
            
            # Choose only the outer contours as the output
            for i,sc1 in enumerate(contour_candidates):
                for j,sc2 in enumerate(contour_candidates):
                    if (i!=j and
                        pyclipper.PointInPolygon(sc1[0], sc2) != 0 and
                        pyclipper.PointInPolygon(sc1[-1], sc2) != 0):
                            break
                else:
                    contours_co[c].append(sc1)
        
        return contours_co, components_label


def xy0(vec, depth=0):
    """Empty the depth for 2D lookup, e.g., KDTree search"""
    return Vector((vec[0],vec[1],depth))

# DepthLookupTree,
class DepthLookupTree:
    """
    Data structure based on KDTree to get depth value from 2D coordinates.
    Some operators have more complicated rules to determine the depth and do not use this class
    """
    def __init__(self, poly_list, depth_list):
        self.co2d = []
        self.depth = []
        self.count = 0
        self.indices = []
        for i,co_list in enumerate(poly_list):
            for j,co in enumerate(co_list):
                self.co2d.append(xy0(co))
                self.depth.append(depth_list[i][j])
                self.indices.append((i,j))
                self.count += 1
        self.kdtree = kdtree.KDTree(self.count)
        for i in range(self.count):
            self.kdtree.insert(self.co2d[i], i)
        self.kdtree.balance()

    def get_depth(self, co):
        _, i, _ = self.kdtree.find(xy0(co))
        return self.depth[i]

    def get_info(self, co):
        """Get stroke index, point index and the distance"""
        _, i, dist = self.kdtree.find(xy0(co))
        return self.indices[i][0], self.indices[i][1], dist       


# get_transformation_mat,

def get_transformation_mat(mode='VIEW', gp_obj=None, strokes=[], operator=None):
    """
    Get the transformation matrix and its inverse matrix given a 2D working plane.
    The x and y values of transformed coordinates will be used for 2D operators.
    """
    presets = {'X-Z': Matrix([[1,0,0],
                              [0,0,1],
                              [0,-1,0]]),
               'Y-Z': Matrix([[0,1,0],
                              [0,0,1],
                              [1,0,0]]),
               'X-Y': Matrix([[1,0,0],
                              [0,1,0],
                              [0,0,1]])}
    
    view_matrix = bpy.context.space_data.region_3d.view_matrix.to_3x3()
    if gp_obj:
        obj_rotation = gp_obj.matrix_world.to_3x3().normalized()
        layer_rotation = gp_obj.data.layers.active.matrix_layer.to_3x3().normalized()
        view_matrix = view_matrix @ obj_rotation
        if bpy.context.scene.nijigp_working_plane_layer_transform:
            view_matrix = view_matrix @ layer_rotation

    # Use orthogonal planes
    if mode in presets:
        mat = presets[mode]
        return mat, mat.inverted_safe()
    
    # Auto mode: use PCA combined with the view vector to determine
    for _ in range(1):
        if mode == 'AUTO':
            data = []
            for stroke in strokes:
                for point in stroke.points:
                    data.append(point.co)
            if len(data) < 2:                           # PCA cannot handle. Use view plane instead.
                break
            
            mat, eigenvalues = pca(np.array(data))
            
            # Cases that result has 1 or 3 dimensions
            if eigenvalues[1] < 1e-6 and eigenvalues[2] < 1e-6:
                break
            if eigenvalues[-1] > 1e-6 and operator:
                operator.report({"INFO"}, "More than one 2D plane detected. The result may be inaccurate.")
            
            mat = Matrix(mat).transposed()

            # Align the result with the current view
            if mat[2].dot(view_matrix[2]) < 0:  
                mat[2] *= -1                          # 1. Make the depth axis facing the screen
            if mat[0].cross(mat[1]).dot(mat[2]) < 0:
                mat[1] *= -1                          # 2. Ensure the rule of calculating normals is consistent
            # 3. Rotate the first two axes to align the UV/tangent
            target_up_axis = mat @ view_matrix.inverted_safe() @ Vector((0,1,0))
            delta = Vector((0,1)).angle_signed(target_up_axis[:2])
            rotation = Matrix.Rotation(delta,3,mat[2])
            mat = mat @ rotation
            return mat, mat.inverted_safe()
        
    # Use view plane
    mat = view_matrix
    if mat[0].cross(mat[1]).dot(mat[2]) < 0:
        mat[1] *= -1 
    return mat, mat.inverted_safe()

# get_2d_bound_box

def get_full_bound_box(s):
    """Bpy provides two bound box points for each stroke, but we need all 8 if transformed
    args: s (bpy.types.GPencilStroke)
    return: bound_points (list of mathutils.Vector)"""
    bound_points = []
    for x in (s.bound_box_min.x, s.bound_box_max.x):
        for y in (s.bound_box_min.y, s.bound_box_max.y):
            for z in (s.bound_box_min.z, s.bound_box_max.z):
                bound_points.append(Vector([x,y,z]))
    return bound_points

def get_2d_bound_box(strokes, t_mat):
    """Get 2D bounds [u_min, v_min, u_max, v_max] of a list of strokes given a 3D-to-2D transformation matrix
    args: strokes (list of bpy.types.GPencilStroke)
          t_mat (mathutils.Matrix)
    return: [u_min, v_min, u_max, v_max]"""
    corners = [None, None, None, None]
    for s in strokes:
        bound_points = get_full_bound_box(s)
        for co in bound_points:
            co_2d = t_mat @ co
            u, v = co_2d[0], co_2d[1]
            corners[0] = u if (not corners[0] or u<corners[0]) else corners[0]
            corners[1] = v if (not corners[1] or v<corners[1]) else corners[1]
            corners[2] = u if (not corners[2] or u>corners[2]) else corners[2]
            corners[3] = v if (not corners[3] or v>corners[3]) else corners[3]
    return corners

# ,get_2d_co_from_strokes,

def get_2d_co_from_strokes(stroke_list, t_mat, scale = False, correct_orientation = False, scale_factor=None, return_orientation = False):
    """
    Convert points from a list of strokes to 2D coordinates. Scale them to be compatible with Clipper.
    Return a 2D coordinate list, a z-depth list and the scale factor 
    """
    poly_list = []
    poly_depth_list = []
    poly_inverted = []
    w_bound = [math.inf, -math.inf]
    h_bound = [math.inf, -math.inf]

    for stroke in stroke_list:
        co_list = []
        depth_list = []
        for point in stroke.points:
            transformed_co = t_mat @ point.co
            co_list.append([transformed_co[0],transformed_co[1]])
            depth_list.append(transformed_co[2])
            w_bound[0] = min(w_bound[0], co_list[-1][0])
            w_bound[1] = max(w_bound[1], co_list[-1][0])
            h_bound[0] = min(h_bound[0], co_list[-1][1])
            h_bound[1] = max(h_bound[1], co_list[-1][1])
        poly_list.append(co_list)
        poly_depth_list.append(depth_list)
        poly_inverted.append(False)

    if scale and not scale_factor:
        poly_W = w_bound[1] - w_bound[0]
        poly_H = h_bound[1] - h_bound[0]
        
        if math.isclose(poly_W, 0) and math.isclose(poly_H, 0):
            scale_factor = 1
        elif math.isclose(poly_W, 0):
            scale_factor = SCALE_CONSTANT / min(poly_H, SCALE_CONSTANT)
        elif math.isclose(poly_H, 0):
            scale_factor = SCALE_CONSTANT / min(poly_W, SCALE_CONSTANT)
        else:
            scale_factor = SCALE_CONSTANT / min(poly_W, poly_H, SCALE_CONSTANT)

    if scale_factor:
        for co_list in poly_list:
            for co in co_list:
                co[0] *= scale_factor
                co[1] *= scale_factor

    # Since Grease Pencil does not care whether the sequence of points is clockwise,
    # Clipper may regard some strokes as negative polygons, which needs a fix 
    if correct_orientation:
        import pyclipper
        for i,co_list in enumerate(poly_list):
            if not pyclipper.Orientation(co_list):
                co_list.reverse()
                poly_inverted[i] = True

    if return_orientation:
        return poly_list, poly_depth_list, poly_inverted, scale_factor
    return poly_list, poly_depth_list, scale_factor


# pad_2d_box,

def pad_2d_box(corners, ratio, return_bounds = False):
    """Scale a 2D box by a given ratio from its center. Return either 4 points or 2 bounds"""
    bound_W, bound_H = corners[2]-corners[0], corners[3]-corners[1]
    if return_bounds:
        return [corners[0] - ratio * bound_W, corners[1] - ratio * bound_H, corners[2] + ratio * bound_W, corners[3] + ratio * bound_H]
    else:
        return [(corners[0] - ratio * bound_W, corners[1] - ratio * bound_H),
                (corners[0] - ratio * bound_W, corners[3] + ratio * bound_H),
                (corners[2] + ratio * bound_W, corners[1] - ratio * bound_H),
                (corners[2] + ratio * bound_W, corners[3] + ratio * bound_H)]

# is_stroke_line
def is_stroke_line(stroke, gp_obj):
    """
    Check if a stroke does not have fill material
    """
    mat_idx = stroke.material_index
    material = gp_obj.material_slots[mat_idx].material
    return not material.grease_pencil.show_fill

# ,rgb_to_hex_code,
def rgb_to_hex_code(color:Color) -> str:
    r,g,b = int(color[0]*255), int(color[1]*255), int(color[2]*255)
    hex_code = f"#{r:02x}{g:02x}{b:02x}"
    return hex_code

# restore_3d_co,
def restore_3d_co(co, depth, inv_mat, scale_factor=1):
    """Perform inverse transformation on 2D coordinates"""
    vec = inv_mat @ Vector([co[0]/ scale_factor,
                            co[1]/ scale_factor,
                            depth])
    return vec

# refresh_strokes
def refresh_strokes(gp_obj, frame_numbers):
    """
    When generating new strokes via scripting, sometimes the strokes do not have correct bound boxes and are not displayed correctly.
    This function recalculates the geometry data
    """
    current_mode = bpy.context.mode
    current_frame = bpy.context.scene.frame_current

    bpy.ops.object.mode_set(mode='EDIT_GPENCIL')
    for f in frame_numbers:
        bpy.context.scene.frame_current = f
        bpy.ops.gpencil.recalc_geometry()
    
    bpy.ops.object.mode_set(mode=current_mode)
    bpy.context.scene.frame_current = current_frame

def pca(data):
    """
    Calculate the matrix that projects 3D shapes to 2D in the optimal way
    """
    mean = np.mean(data, axis=0)
    centered_data = data - mean
    covariance_matrix = np.cov(centered_data, rowvar=False)
    eigenvalues, eigenvectors = np.linalg.eig(covariance_matrix)

    idx = eigenvalues.argsort()[::-1]  
    values =  eigenvalues[idx]
    mat = eigenvectors[:,idx]
    return mat, values