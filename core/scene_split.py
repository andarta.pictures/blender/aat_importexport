# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
from bpy.types import Operator
import re

from ..common import gp_utils

from .. import core

pop = lambda l : l[0] if len(l) > 0 else None


def datalink_object(gp_object, new_name, collection) :
    """Clones an object into a new object copy with a specified name 
    and adds it to a collection. The cloned object will have its own location 
    and other properties but will share the same data as the original.

    Parameters:
    - gp_object (bpy.types.Object): The object to be cloned.
    - new_name (str): The name for the new object.
    - collection (bpy.types.Collection): The collection to which the new object will be added.
    """

    data = gp_object.data

    if type(data).__name__ == "Image" :
        new_gp = bpy.data.objects.new(new_name, None)
        new_gp.data = data
    
    else :
        new_gp = bpy.data.objects.new(new_name, data)
    
    new_gp.matrix_world = gp_object.matrix_world.copy()
    collection.objects.link(new_gp)


def get_clean_collection(scene) :
    """Retrieves the collection with the name containing 'CLEAN' within the scene's hierarchy.

    Parameters:
    - scene (bpy.types.Scene): The Blender scene.

    Returns:
    - clean_collection (bpy.types.Collection): The collection with the name containing 'CLEAN.'
    """

    for col in scene.collection.children_recursive : 
        if "CLEAN" in col.name and "BG" in col.name :
            return col


def get_paint_collection(scene) :
    for col in scene.collection.children_recursive : 
        if "PAINT" in col.name and "BG" in col.name :
            return col

def link_active(scene) :
    clean_col = get_clean_collection(scene)
    if clean_col is None : 
        return False
    
    paint_col = get_paint_collection(scene)
    if paint_col is None : 
        return False
    
    clean_lay_col = gp_utils.get_layer_collection(clean_col)
    if clean_lay_col.exclude : 
        if len(clean_col.all_objects) == len(paint_col.all_objects) : 
            return True

    return False


def get_cam_collection(scene):
    """Retrieves the collection with the name containing 'CAM' within the scene's hierarchy.

    Parameters:
    - scene (bpy.types.Scene): The Blender scene.

    Returns:
    - cam_collection (bpy.types.Collection): The collection with the name containing 'CAM.'"""
    
    for col in scene.collection.children_recursive : 
        if "CAM" in col.name  and "RDR" in col.name  :
            return col
        

def unlink_from_scene(object, scene) :
    """Unlinks an object from the scene by removing it from its associated collections.

    Parameters:
    - object (bpy.types.Object): The Blender object to be unlinked.
    - scene (bpy.types.Scene): The Blender scene.
    """
    for collection in scene.collection.children_recursive :
        if object.name in [o.name for o in collection.objects] :
            collection.objects.unlink(object)
        

def unlink_from_collection(object, collection) :
    if object.name in [o.name for o in collection.objects] :
        collection.objects.unlink(object)


def get_collection_name(name, scene) :
    shot_code = scene.name.replace("SCN_", "")

    # one liner to remove any .001, .002 etc ending
    remove_serial_ending = lambda s : re.sub(r"\.[0-9]*$", "", s) 
    base_name = remove_serial_ending(name)
    
    return get_name(base_name, shot_code)


def cleanup_unused_objects() :
    """Cleans up any object that is not linked to any scene."""

    cleanup_log = []

    for o in bpy.data.objects :
        if len(o.users_collection) == 0 :
            cleanup_log.append(o.name)
            bpy.data.objects.remove(o, do_unlink=True)
    
    print("%d unused objects cleaned up." % len(cleanup_log))


def cleanup_unused_collections() :
    """Cleans up any collection that is linked to no scene at all."""

    cleanup_log = []

    names = lambda l : [e.name for e in l]

    for col in bpy.data.collections : 
        linked_scenes = [scene for scene in bpy.data.scenes if col.name in names(scene.collection.children_recursive)]
        if len(linked_scenes) == 0 :
            cleanup_log.append(col.name)
            bpy.data.collections.remove(col)
    
    print("%d unused collections cleaned up." % len(cleanup_log))


def cleanup() :
    """Cleans up unused objects and collections."""
    cleanup_unused_collections()
    cleanup_unused_objects()


def get_name(name, shot_code) :
    standard_name = re.sub(r"S[QC][0-9]*_SH[0-9]*", "####", name) 
    standard_name = standard_name.replace("BUILD", "####")

    if "####" in standard_name : 
        return standard_name.replace("####", shot_code)

    else :
        return standard_name+"_"+shot_code
    

def datalink_collection(src_collection, dest_collection, shot_code) :
    # print("%s content : " % src_collection.name, [type(o.data).__name__ for o in src_collection.objects])
    datalinkable_objects = lambda col : [o for o in col.objects if type(o.data).__name__ not in ['Camera', 'Image']]

    for o in datalinkable_objects(dest_collection) :
        unlink_from_collection(o, dest_collection)

    for o in datalinkable_objects(src_collection) :
        new_name = get_name(o.name, shot_code)

        if new_name in bpy.data.objects.keys() :
            print("Warning : %s already exists." % new_name)
            continue
        
        datalink_object(o, new_name, dest_collection)


def update_frame_range(scene, shot_code) :
    marker = [m.frame for m in scene.timeline_markers if shot_code in m.name]

    if len(marker) > 0 :
        print("Marker found for %s : " % shot_code, marker[0])
        scene.frame_start = 1
        scene.frame_end = marker[0]
    
    else :
        print("Marker not found for %s" % shot_code)
    
    scene.timeline_markers.clear()

    


### PUBLIC ###


def split_scenes() :

    build_scene = core.scene_shift.get_build_scene()
    cameras = [obj for obj in build_scene.objects if obj.type == 'CAMERA']

    if len(cameras) < 2 :
        assert False, "Not enough cameras in the scene, at least 2 are needed to warrant a scene split."

    # Create a list of shots based on camera names
    shots = ["SCN_" + "_".join(camera.name.split('_')[1:]) for camera in cameras]
    new_scenes = []

    cleanup()

    for shot in shots:
        core.switch_scene(build_scene)
        bpy.ops.scene.new(type='FULL_COPY')

        new_scene = bpy.context.window.scene
        new_scene.name = shot
        new_scenes.append(new_scene)

    shotcode = lambda scene : scene.name.replace("SCN_", "")

    for scene in new_scenes:
        update_frame_range(scene, shotcode(scene))
        for collection in scene.collection.children_recursive :
            collection.name = get_collection_name(collection.name, scene)

    for src_col in build_scene.collection.children_recursive : 
        for scene in new_scenes:
            dst_col_name = get_collection_name(src_col.name, scene)
            dst_col = bpy.data.collections[dst_col_name]
            datalink_collection(src_col, dst_col, shotcode(scene))

    # Move camera to the right collection   

    for scene in new_scenes:
        shot_code = scene.name.replace("SCN_", "")

        for o in scene.objects :
            if o.type == 'CAMERA' :
                unlink_from_scene(o, scene)
            if type(o.data).__name__ == 'Image' :
                o.name = get_collection_name(o.name, scene)

        cam_col = get_cam_collection(scene)
        matching_camera = [cam for cam in cameras if shot_code in cam.name][0]
        cam_col.objects.link(matching_camera)
    
    cleanup()


def send_to_scene(object, dest_scene) :


    collections = bpy.context.scene.collection.children_recursive
    names = lambda l : [e.name for e in l]
    og_col = pop([c for c in collections if object.name in names(c.objects)])

    dst_col = None
    if og_col is not None :
        dst_col_name = get_collection_name(og_col.name, dest_scene)
        if dst_col_name in bpy.data.collections.keys() : 
            dst_col = bpy.data.collections[dst_col_name]
    
    else : 
        dst_col = dest_scene.collection
    
    print("Destination collection : ", dst_col.name)

    dest_scene_object_names = [o.name for o in dest_scene.collection.all_objects]

    shot_code = dest_scene.name.replace("SCN_", "")
    new_name = get_name(object.name, shot_code)
    if new_name in dest_scene_object_names :
        print("%s already in %s" % (new_name, dest_scene.name))
        return
    # assert new_name not in dest_scene_object_names, "%s already exists in %s." % (object.name, dest_scene.name)
    print("Cloning : %s >> %s" % (object.name, new_name))

    datalink_object(object, new_name, dst_col)