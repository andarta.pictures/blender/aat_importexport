# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import math

from bpy.props import BoolProperty, CollectionProperty, StringProperty, PointerProperty

from .common.utils import register_classes, unregister_classes
from .common.empty_display import destroy_empties
from .common.keymaps import register_keymap
from .common import blender_ui, log_3D, gp_utils, timer
from .common import empty_display as empties

from . import core



class GPEDIT_OT_IO_update_export_camera(bpy.types.Operator):
    bl_idname = "anim.gp_io_update_export_camera"
    bl_label = "Update Export Camera"
    bl_options = {'REGISTER', 'UNDO'}

    
    @classmethod
    def poll(cls, context):
        return "Export_Camera" in bpy.data.objects.keys()

    def execute(self, context):
        export_cam = bpy.data.objects["Export_Camera"]
        core.export_camera.restore_resolution()
        
        build_scene = core.scene_shift.get_build_scene()
        combined_cluster = core.projection_clusters.get_current_combined_cluster()
        combined_cluster = combined_cluster.cast_into_scene(build_scene)

        if log_3D.get_display() == True :
            log_3D.destroy_3D_logs()
            for o_name in combined_cluster.get_object_list() :
                object_cluster = combined_cluster.filter_by_object(o_name)
                object_cluster = object_cluster.crop_to_geometry()
                object_cluster.display()
        
        
        export_cam.location = core.export_camera.get_camera_best_position(combined_cluster, depth=export_cam.location.y)
        core.export_camera.update_export_camera_aperture(export_cam, combined_cluster, tilt=True)
        export_cam.rotation_euler = (math.pi/2, 0, 0)

        return {'FINISHED'}


class GPEDIT_OT_IO_render_export_camera(bpy.types.Operator):
    bl_idname = "anim.gp_io_render_export_camera"
    bl_label = "Render Export Camera"
    bl_options = {'REGISTER', 'UNDO'}

    skip_modal : BoolProperty(default=False)

    def modal(self, context, event):
        
        if event.type == 'TIMER':
            
            if self.render_i < len(self.render_objects) :
                o = self.render_objects[self.render_i]
                self.update_render_status()
                core.render.render_object(o)
                self.render_i += 1
            
            else : 
                self.conclude()
                self.poll(context)
                return {'FINISHED'}

        return {'PASS_THROUGH'}

    def conclude(self) :
        core.render.restore_render_config(self.conf)
        core.render.render_status = ""
        blender_ui.full_redraw()

    def update_render_status(self) :
        o = self.render_objects[self.render_i]
        core.render.render_status = "%s (%d/%d)"%(o.name, self.render_i+1, len(self.render_objects))
    
    @classmethod
    def poll(cls, context):
        return "Export_Camera" in bpy.data.objects.keys() and core.render.render_status == ""
    
    def execute(self, context):
        export_cam = bpy.data.objects["Export_Camera"]
        bpy.context.scene.camera = export_cam
        
        self.render_objects = core.render.get_project_render_list()
        print("Object to be rendered : ", [o.name for o in self.render_objects])
        self.render_i = 0

        self.update_render_status()

        self.conf = core.render.apply_render_config()

        core.data_export.write_export_data(self.render_objects)

        if self.skip_modal :
            for o in self.render_objects :
                core.render.render_object(o)
            self.conclude()
            return {'FINISHED'}

        wm = context.window_manager
        self._timer = wm.event_timer_add(time_step=0.2, window=context.window)
        wm.modal_handler_add(self)
        return {'RUNNING_MODAL'}

class GPEDIT_OT_IO_cancel_export_camera(bpy.types.Operator):
    bl_idname = "anim.gp_io_cancel_export_camera"
    bl_label = "Cancel Export Camera"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        log_3D.destroy_3D_logs()
        core.export_camera.restore_resolution()
        destroy_empties()
        
        if "Export_Camera" in bpy.data.objects.keys() :
            export_cam = bpy.data.objects["Export_Camera"]
            core.export_camera.remove_export_camera(export_cam)
            core.activate_camera_view()

        return {'FINISHED'}


class GPEDIT_OT_IO_restore_export_camera(bpy.types.Operator):
    bl_idname = "anim.gp_io_restore_export_camera"
    bl_label = "Restore Export Camera"
    bl_options = {'REGISTER', 'UNDO'}

    filepath : bpy.props.StringProperty(subtype="FILE_PATH")

    def invoke(self, context, event) :
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        print("File selected : ", self.filepath)
        core.export_camera.restore_export_camera(self.filepath)
        return {'FINISHED'}


class GPEDIT_OT_IO_test(bpy.types.Operator):
    bl_idname = "anim.gp_io_test"
    bl_label = "GP Object TEST op"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        print("Totally testing important stuff.")
        # core.test()

        bpy.ops.anim.gp_io_export_cam_menu(show_dialog=False)

        return {'FINISHED'}
    
class GPEDIT_OT_IO_display_3D_logs(bpy.types.Operator):
    bl_idname = "anim.gp_display_3d_logs"
    bl_label = "Display/Hide 3D logs"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        new_status = not log_3D.get_display()
        print('Setting Log3D display to ', new_status)
        log_3D.set_display(new_status)
        blender_ui.full_redraw()
        return {'FINISHED'}

class GPEDIT_OT_IO_refresh_plane_textures(bpy.types.Operator):
    bl_idname = "anim.gp_io_refresh_plane_textures"
    bl_label = "Refresh Image Textures"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context) :
        conditions = [
            len(core.texture_plane.get_available_asset_packs()) > 0,
            len(context.scene.assets_pack_list) > 0,
        ]
        return False not in conditions

    def execute(self, context):
        print(f"Current asset pack : '{context.scene.assets_pack_list}'")

        eligible_textures = [im for im in bpy.data.images if "-assets" in im.filepath]
        if len(eligible_textures) == 0 :
            blender_ui.show_message("No valid texture was found to be refreshed, please recheck your PSD link.", "ERROR")
            return {'CANCELLED'}

        core.texture_plane.update_image_asset_folder()
        core.texture_plane.reload_modified_images()
        blender_ui.full_redraw()

        msg = core.texture_plane.sanity_check_image_asset_size()
        if msg is not None :
            blender_ui.show_message(msg, "Error during texture refresh", "ERROR")
            return {'CANCELLED'}

        return {'FINISHED'}

class GPEDIT_OT_IO_link_assets(bpy.types.Operator):
    bl_idname = "anim.gp_io_link_assets"
    bl_label = "Link Photoshop Assets"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context) :
        conditions = [
            len(core.texture_plane.get_available_asset_packs()) > 0,
            len(context.scene.assets_pack_list) > 0,
        ]
        return False not in conditions

    def execute(self, context):

        if "Export_Camera" not in bpy.data.objects.keys() :
            blender_ui.show_message("Could not find Export_Camera.")
            return {'CANCELLED'}
        
        try :
            export_cam = bpy.data.objects["Export_Camera"]
            bpy.context.scene.camera = export_cam
            core.activate_camera_view()

            core.texture_plane.import_image_assets()
            core.switch_scene(core.scene_shift.get_build_scene())

            return {'FINISHED'}
        
        except AssertionError as e :
            blender_ui.show_message(e.args[0], "Error during photoshop linking")
            print("\n"+e.args[0])
            return {'CANCELLED'}

class CameraSelect (bpy.types.PropertyGroup) :
    camera : PointerProperty(name="Camera", type=bpy.types.Object)
    select : BoolProperty(default=False)
    

class GPEDIT_OT_IO_open_export_camera_menu(bpy.types.Operator):
    bl_idname = "anim.gp_io_export_cam_menu"
    bl_label = "Export Cameras to Photoshop"
    bl_options = {'REGISTER', 'UNDO'}

    show_dialog : BoolProperty(default=True)

    def invoke(self, context, event):
        scene = context.scene
        old_selection = [e.camera.name for e in scene.selected_cameras if e.camera is not None and e.select]
        scene.selected_cameras.clear()

        for cam in core.get_all_cameras() :
            cam_entry = scene.selected_cameras.add()
            cam_entry.camera = cam
            cam_entry.select = cam.name in old_selection

        if not self.show_dialog :
            self.execute(context)
            return {'FINISHED'}

        wm = bpy.context.window_manager
        wm.invoke_props_dialog(self, width=150)
        return {'RUNNING_MODAL'}
    

    def draw(self, context):
        layout = self.layout
        camera_list = context.scene.selected_cameras
        
        for cam_entry in camera_list :
            row = layout.row()
            row.prop(cam_entry, "select", text=cam_entry.camera.name)


    def execute(self, context) :

        try :

            build_scene = core.scene_shift.get_build_scene()
            core.switch_scene(build_scene)
            core.activate_camera_view()

            build_scene.original_resolution = (bpy.context.scene.render.resolution_x, bpy.context.scene.render.resolution_y)

            print("\nCalculating cluster.")
            combined_cluster = core.projection.get_current_combined_cluster()
            print(f"{len(combined_cluster.data)} projections calculated.")
            
            print("\nCasting cluster to SCN_BUILD.")
            combined_cluster = combined_cluster.cast_into_scene(build_scene)
            print(f"{len(combined_cluster.data)} projections left.")

            if log_3D.get_display() == True :
                for o_name in combined_cluster.get_object_list() :
                    object_cluster = combined_cluster.filter_by_object(o_name)
                    object_cluster = object_cluster.crop_to_geometry()
                    object_cluster.display()
            
            assert len(combined_cluster.data) > 0, "Error : no projections were found."

            core.export_camera.initialize_export_camera(combined_cluster)
        
        except AssertionError as e :
            blender_ui.show_message(e.args[0], "Error during export camera creation")
            print("\n"+e.args[0])
            return {'CANCELLED'}

        return {'FINISHED'}
    


class Build_OT_Split_Scenes(bpy.types.Operator):
    """create new scene and link objects"""
    bl_idname = "object.build_decors"
    bl_label = "build decors"

    def execute(self, context):
        try :
            core.scene_split.split_scenes()
        
        except AssertionError as e :
            blender_ui.show_message(e.args[0], "Error")
            print("\n"+e.args[0])
            return {'CANCELLED'}

        return {'FINISHED'}
    


class Build_OT_Simplfied_UI_infos(bpy.types.Operator):
    """Simplfied UI info box"""
    bl_idname = "anim.simplified_ui_infos"
    bl_label = "Simplified UI infos"

    def execute(self, context):
        blender_ui.show_message("Please reenable CLEAN collection to access full UI.", "Info")
        return {'FINISHED'}
    


class BUILD_scene_select(bpy.types.PropertyGroup) :
    name : bpy.props.StringProperty(name="Name", default="Unknown")
    selected : bpy.props.BoolProperty(name="Selected", default=False)



class Build_OT_send_to_scene(bpy.types.Operator):
    """Links a grease pencil object to one or more scenes with the appropriate name"""
    bl_idname = "object.clone_to_scene"
    bl_label = "Link object to new scene"

    scene_selection : bpy.props.CollectionProperty(type=BUILD_scene_select)

    def invoke(self, context, event):
        self.scene_selection.clear()
        for scene in bpy.data.scenes :
            scene_prop = self.scene_selection.add()
            scene_prop.name = scene.name

        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=150)
    
    def draw(self, context):
        layout = self.layout

        for scene_prop in self.scene_selection :
            layout.prop(scene_prop, "selected", text=scene_prop.name)

    def execute(self, context):
        
        scenes = [bpy.data.scenes[s.name] for s in self.scene_selection if s.selected]
        objects = bpy.context.selected_objects

        print("Selected scenes : ", [s.name for s in scenes])
        print("Selected objects : ", [o.name for o in objects])
        
        for s in scenes :
            for o in objects :
                core.scene_split.send_to_scene(o,s)
                
        return {'FINISHED'}
        


classes = [
    GPEDIT_OT_IO_test,
    GPEDIT_OT_IO_update_export_camera,
    GPEDIT_OT_IO_render_export_camera,
    GPEDIT_OT_IO_cancel_export_camera,
    GPEDIT_OT_IO_refresh_plane_textures,
    GPEDIT_OT_IO_link_assets,
    GPEDIT_OT_IO_open_export_camera_menu,
    GPEDIT_OT_IO_display_3D_logs,
    GPEDIT_OT_IO_restore_export_camera,
    Build_OT_Split_Scenes,
    BUILD_scene_select,
    Build_OT_send_to_scene,
    CameraSelect,
    Build_OT_Simplfied_UI_infos,
]

def register() :
    register_classes(classes)
    register_keymap(GPEDIT_OT_IO_refresh_plane_textures.bl_idname, "R", ctrl=True, value='PRESS', space_type="VIEW_3D")
    bpy.types.Scene.selected_cameras = CollectionProperty(name="Selected Cameras", type=CameraSelect)

def unregister() :
    del bpy.types.Scene.selected_cameras
    unregister_classes(classes)