# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

from .. import core
from .data_export import create_safe_directory, get_export_path
from ..common import gp_utils

render_status = ""


def get_all_visible_GP_Objects(scene=None) :
    """
    Returns a list of all render-visible Grease Pencil objects.

    Parameters:
    - scene (bpy.types.Scene, optional): The Blender scene. If not provided, uses the active scene.

    Returns:
    - objects (list): List of render-visible Grease Pencil objects.
    
    """
    
    objects = []

    if scene is None :
        scene = bpy.context.scene

    for obj in scene.objects : 
        if not obj.hide_render :
            if isinstance(obj.data, bpy.types.GreasePencil) :
                objects.append(obj)

    return objects


def get_all_visible_objects() :
    """Returns a list of all render-visible objects.

    Returns:
    - objects (list): List of render-visible objects.
    
    """

    objects = []

    scene = bpy.context.scene

    for obj in scene.objects : 
        if not obj.hide_render :
            objects.append(obj)
    
    return objects


def remove_disabled_objects(objects, viewlayer) :
    """ Removes objects from the list if they're disabled in the provided viewlayer.

    Parameters:
    - objects (list): List of objects.
    - viewlayer (bpy.types.ViewLayer): The Blender view layer.

    Returns:
    - list: filter list of objects.

    """

    return [o for o in objects if gp_utils.is_collection_hierarchy_active(o, viewlayer)]


def count_points(gp) : 
    count = 0
    for l in gp.data.layers : 
        for f in l.frames : 
            for s in f.strokes :
                count += len(s.points)
    return count


def get_renderable_objects(scene=None) :
    """ Returns a list of all renderable Grease Pencil objects.

    Parameters:
    - scene (bpy.types.Scene, optional): The Blender scene. If not provided, uses the active scene.

    Returns:
    - objects (list): List of renderable Grease Pencil objects.

    """

    objects = []

    if scene is None :
        scene = bpy.context.scene

    for obj in scene.objects : 
        if not obj.hide_render :
            if isinstance(obj.data, bpy.types.GreasePencil) :
                objects.append(obj)
    
    return objects


def get_project_render_list() :
    """Returns a list of all renderable Grease Pencil objects."""

    names = lambda lst : [o.name for o in lst]
    unique = lambda lst : list(set(lst))

    if len(bpy.data.scenes) == 1 or "SCN_BUILD" not in names(bpy.data.scenes) :
        renderable_objects = get_renderable_objects(bpy.context.scene)
        renderable_objects = remove_disabled_objects(renderable_objects, bpy.context.scene.view_layers[0])

    else :
        build_scene = core.scene_shift.get_build_scene()
        build_objects = get_renderable_objects(build_scene)
        build_objects = remove_disabled_objects(build_objects, build_scene.view_layers[0])

        exported_cams = core.get_exported_cams()
        exported_scenes = [core.scene_shift.get_camera_original_scene(c) for c in exported_cams]
        exported_scenes = unique(exported_scenes)

        print("Build objects : ", [o.name for o in build_objects])

        scene_objects_data_names = []
        for scene in exported_scenes :
            objects = get_renderable_objects(scene)
            for o in objects :
                scene_objects_data_names.append(o.data.name)

        renderable_objects = [o for o in build_objects if o.data.name in scene_objects_data_names]
        print("Objects after scene filtering : ", [o.name for o in renderable_objects])

    renderable_objects = [o for o in renderable_objects if count_points(o) > 1]
    print("Objects after point filtering : ", [o.name for o in renderable_objects])
    return renderable_objects
        

def render_viewport(output_file) :
    """Renders viewport to specified output_file as a PNG."""
    bpy.context.scene.render.image_settings.file_format = "PNG"
    bpy.ops.render.render(write_still=False)
    bpy.data.images["Render Result"].save_render(output_file)


def set_hide_render(value, objects) :
    """Sets hide_render value for all objects in objects list."""
    for o in objects :
        o.hide_render = value


def set_use_light(value, objects) :
    """Sets use_lights value for all objects in objects list."""
    for o in objects : 
        for l in o.data.layers :
            l.use_lights = value


def set_hide_layer(status, layer_list) :
    """Sets hide value for every layer in layer_list."""
    for l in layer_list :
        l.hide = status


def apply_render_config() : 
    """Temporarily applies a number of configuration 
    changes to Blender to prepare for render.

    Returns:
    - og_configs (list): Original configuration settings. 
    Can be used by restore_render_config() after render."""

    og_configs = []

    for scene in bpy.data.scenes : 
        conf = [
            "SCENE",
            scene, 
            scene.render.film_transparent, 
            scene.render.image_settings.file_format, 
            scene.render.image_settings.color_mode
            ]
        og_configs.append(conf)
        
        scene.render.film_transparent = True
        scene.render.image_settings.file_format = "PNG"
        scene.render.image_settings.color_mode = "RGBA"

    for world in bpy.data.worlds :
        conf = [
            "WORLD",
            world,
            world.node_tree.nodes["Background"].inputs[0].default_value,
            world.node_tree.nodes["Background"].inputs[1].default_value,
            ]
        og_configs.append(conf)

        world.node_tree.nodes["Background"].inputs[0].default_value = (1.0, 1.0, 1.0, 0.0)
        world.node_tree.nodes["Background"].inputs[1].default_value = 1.0
    
    return og_configs


def restore_render_config(confs) :
    """
    Restores the Blender configuration to its original state after render.

    Parameters:
    - confs (list): Original configuration settings (obtained via apply_render_config return)
    """

    for c in confs :
        if c[0] == "SCENE" : 
            scene = c[1]
            scene.render.film_transparent = c[2]
            scene.render.image_settings.file_format = c[3]
            scene.render.image_settings.color_mode = c[4]
        
        if c[0] == "WORLD" :
            world = c[1]
            world.node_tree.nodes["Background"].inputs[0].default_value = c[2]
            world.node_tree.nodes["Background"].inputs[1].default_value = c[3]


def render_object(render_object) :
    """Renders a specified Grease Pencil object."""
    global render_status

    path = get_export_path() + "GP_renders"

    visible_objects = get_all_visible_objects()
    set_use_light(False, [render_object])

    set_hide_render(True, visible_objects)
    set_hide_render(False, [render_object])

    visibles_layers = [l for l in render_object.data.layers if l.hide == False]

    for l in visibles_layers : 
        set_hide_layer(True, visibles_layers)
        set_hide_layer(False, [l])

        img_path = "%s\\%s_%s.png" % (path, render_object.name, l.info)
        print("Rendering : ", img_path)
        
        render_viewport(img_path)
    
    set_hide_layer(False, visibles_layers)
    set_hide_render(False, visible_objects)