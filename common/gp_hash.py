import hashlib
import array

from ..common import timer


def gp_data_length(gp) :
    n = 0
    for l in gp.data.layers :
        for f in l.frames : 
            for s in f.strokes : 
                n += len(s.points)
    return n


def pick_points(gp, pickrate, frame_n=None) : 
    picked = []

    size = gp_data_length(gp)
    step = max(1,int(size*pickrate))

    for l in gp.data.layers :
        for f in l.frames : 
            if frame_n is None or frame_n == f.frame_number : 
                for s in f.strokes : 
                    stroke_length = len(s.points)
                    j = 0
                    while j < stroke_length :
                        picked += list(s.points[j].co)
                        j += step
    
    return picked


# def pick_points() : 
#     size = gp_data_length(gp)
#     step = max(1,int(size*pickrate))
#     points = pick_points(gp, step)


def get_animation_data(gp) :
    anim_data = []

    if gp.animation_data is not None and gp.animation_data.action is not None:
        for fcurve in gp.animation_data.action.fcurves:
            if fcurve.data_path in ["location", "rotation_euler", "scale"] :
                keyframes = [(kf.co.x, kf.co.y) for kf in fcurve.keyframe_points]
                anim_data.append(("%s[%d]" % (fcurve.data_path, fcurve.array_index), keyframes))
    
    anim_data.sort(key=lambda e : e[0])

    return anim_data


def assert_validity(obj) : 
    if type(obj).__name__ != "Object" :
        raise ValueError(f"Provided object {str(obj)} is not a Blender object ({type(obj).__name__})")



### PUBLIC ###


def hash(gp, pickrate=0.02, hex=False, tolerate_objects=True) :
    assert_validity(gp)
    
    digest = lambda h : h.hexdigest() if hex else h.digest()
    h = hashlib.sha256()

    anim_data = str(get_animation_data(gp))
    h.update(anim_data.encode())

    if type(gp.data).__name__ != "GreasePencil" :
        if tolerate_objects :
            return digest(h)
        else :
            raise ValueError(f"Provided object doesn't have a legal type for hashing ({type(gp.data).__name__})")

    points = pick_points(gp, pickrate)

    points_hashable = array.array('f', points)
    h.update(points_hashable)

    visible_layers = [(l.info, l.opacity) for l in gp.data.layers if not l.hide]
    h.update(str(visible_layers).encode())

    return digest(h)

def hash_frame(object_list, frame_n, pickrate=0.02, hex=False) : 
    h = hashlib.sha256()
    digest = lambda h : h.hexdigest() if hex else h.digest()

    for o in object_list :
        assert_validity(o)
        matrix_world = str(o.matrix_world)
        h.update(matrix_world.encode())

        if type(o.data).__name__ == "GreasePencil" :
            points = pick_points(o, pickrate, frame_n=frame_n)
            points_hashable = array.array('f', points)
            h.update(points_hashable)

    return digest(h)
