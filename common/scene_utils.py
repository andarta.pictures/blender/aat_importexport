# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy


def get_object_alias(object, scene) :
    """Returns an object that shares the same data as object argument in 
    a specific scene, None if no corresponding object was found."""
    
    for o in scene.objects :
        if o.data is not None :
            if object.data.name == o.data.name :
                return o
