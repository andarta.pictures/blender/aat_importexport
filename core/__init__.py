# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import math
import numpy as np
import bmesh

from ..common.gp_utils import get_all_gp_objects, get_scene_gp_objects
from ..common.empty_display import display_angle, create_empty
from ..common.geometry_3D import print_m
from ..common import mode_manager, log_3D

from . import export_camera, projection_clusters as projection
from . import render, data_export, texture_plane, camera_angles, scene_shift, scene_split
from .. import core

from .projection_clusters import ProjectionCluster


def activate_camera_view() :
    """Puts user in camera view."""
    for area in bpy.context.screen.areas:
        if area.type == 'VIEW_3D':
            area.spaces[0].region_3d.view_perspective = 'CAMERA'
            break

def get_all_cameras() :
    cameras = []
    camera_names = []

    for o in bpy.data.objects : 
        if isinstance(o.data, bpy.types.Camera) and o.name not in camera_names :
            cameras.append(o)
            camera_names.append(o.name)
    
    return cameras

# def is_scene_tagged_for_export(scene) :
#     for o in scene.objects :
#         if isinstance(o.data, bpy.types.Camera) :
#             if o.data.photoshop_export :
#                 return True
#     return False


def get_exported_cams() :
    cam_list = bpy.context.scene.selected_cameras
    return [c.camera for c in cam_list if c.select]


def get_exported_cams_max_depth() :
    scene = bpy.context.scene
    og_frame = scene.frame_current
    cam_y = []

    for c in get_exported_cams() :
        for i in range(scene.frame_start, scene.frame_end+1) :
            scene.frame_set(i)
            cam_y.append(c.location[1])

    scene.frame_set(og_frame)

    max_depth = min(cam_y)
    return max_depth


def switch_scene(scene) :
    window = bpy.context.window
    og_scene = window.scene
    window.scene = scene
    bpy.context.view_layer.update()
    print("\nSwitching to %s" % scene.name)
    return og_scene


def test() :
    print("\n### TEST ###")

    scene = bpy.context.scene
    print('Timeline markers : ', [(m.name, m.frame) for m in scene.timeline_markers])



def register() :
    bpy.types.Scene.original_resolution = bpy.props.IntVectorProperty(name="original_resolution", size=2)
    bpy.types.Scene.original_camera = bpy.props.StringProperty(name="original_camera")


def unregister() :
    del bpy.types.Scene.original_resolution
    del bpy.types.Scene.original_camera