# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import math
import numpy as np
from mathutils import Vector

from ..common.gp_utils import get_geo
from ..common.empty_display import display_angle

from .projection_clusters import ProjectionCluster

from .. import core



def update_step_factor(step_factor, current_step, previous_step) : 
    """Cuts step_factor in half if there was a change of sign between 
    the last two steps to avoid ricocheting around instead of 
    converging toward target value."""

    step_factor = step_factor.copy()
    
    step_X, step_Z = current_step
    prev_step_X, prev_step_Z = previous_step

    if step_X * prev_step_X < 0 :
        step_factor[0] = step_factor[0]/2
    
    if step_Z* prev_step_Z < 0 :
        step_factor[1] = step_factor[1]/2  

    return step_factor


def get_camera_best_position(projection_clusters : ProjectionCluster, depth=-12.0) :
    """Iterates the camera position to get the minimal possible aperture while 
    keeping the camera at a set depth and aligned with Y-axis. Stops after a 
    set number of loops or when error goes under a threshold.

    - cam :  camera (not export one)
    - projection_clusters : cf import_export.core.projection_clusers
    
    Returns camera location with minimal FOV"""

    cam_position = [0.0,depth,0.0]

    last_step = None
    step_factor = [10,10]

    cropped_clusters = projection_clusters.crop_to_geometry()

    for i in range(0, 100) :

        alpha1, alpha2, beta1, beta2 = core.camera_angles.get_aperture(cropped_clusters, cam_position, crop_to_geometry=False)

        step_X = (abs(alpha2)-abs(alpha1))*step_factor[0]
        step_Z = (abs(beta2)-abs(beta1))*step_factor[1]
        step_size = step_X**2 + step_Z**2
        
        if last_step is not None :
            step_factor = update_step_factor(step_factor, (step_X, step_Z), last_step)
        last_step = (step_X, step_Z)  

        if step_size < 0.00001 :
            print("Valid camera position found after %d steps."%i)
            break

        print("Step=(%.2f, %.2f), size=%.5f, step_factor=(%.2f, %.2f)"%(step_X, step_Z, step_size, step_factor[0], step_factor[1]))

        cam_position[0] += step_X
        cam_position[2] += step_Z
    
    print("Final cam position : ", cam_position)
    return tuple(cam_position)


def add_alpha_margins(camera_aperture, frame_max_aperture, security_ratio) :
    """Returns a new aperture with a security margin added corresponding to a percentage of the widest shot FOV_angle.
    
    - camera_aperture : (alpha1, alpha2, beta1, beta2) angle of view of the camera
    - security_ratio : ratio of the widest shot to use as margins (0.2 recommended)
    - render_cam_location : location of the export camera
    - alpha_range : tuples of two angles that will be used as boundaries to exclude 
    angles outside of them (useful to exclude angles outside of camera FOV)

    Returns new (alpha, beta) tuple, updated with margins."""

    scene = bpy.context.scene

    aspect_ratio = scene.original_resolution[0]/scene.original_resolution[1]

    alpha_widest = frame_max_aperture[1]-frame_max_aperture[0]
    beta_widest = np.arctan(np.tan(alpha_widest)/aspect_ratio)

    margin = security_ratio*(alpha_widest+beta_widest)/2
    secure_aperture = [
        camera_aperture[0]-margin/2,
        camera_aperture[1]+margin/2,
        camera_aperture[2]-margin/2,
        camera_aperture[3]+margin/2,
    ]

    return secure_aperture



def create_camera(name, location, collection) :
    """Creates a camera at specified location, with specified name, in a specified collection."""
    cam1 = bpy.data.cameras.new(name)
    cam1.clip_end = 50000.0

    cam_obj1 = bpy.data.objects.new(name, cam1)
    cam_obj1.location = location
    
    print("Linking camera to : ", collection.name)
    collection.objects.link(cam_obj1)

    return cam_obj1


def get_resolution(cam_aperture, min_aperture) :
    """Calculates resolution.
    
    - projection_cluster : cf import_export.core.projection_clusters
    - render_cam_location : location of the export camera
    - aperture : (alpha, beta) tuple containing horizontal and vertical field of view of the camera
    
    Returns (res_X, res_Y) resolution of the frame."""

    alpha = cam_aperture[1]-cam_aperture[0]
    beta = cam_aperture[3]-cam_aperture[2]

    alpha_min = min_aperture[1]-min_aperture[0]
    
    res_X = int(1920*np.tan(alpha/2)/np.tan(alpha_min/2))
    res_Y = int(1920*np.tan(beta/2)/np.tan(alpha_min/2))

    return (res_X, res_Y)


def get_COLDEV(scene) :
    scene_collection_names = [col.name for col in scene.collection.children_recursive]

    if "COL-DEV" not in bpy.data.collections :
        collection = bpy.data.collections.new(name='COL-DEV')
    
    collection = bpy.data.collections["COL-DEV"]

    if "COL-DEV" not in scene_collection_names :
        scene.collection.children.link(collection)
    
    return collection
    

def initialize_export_camera(projection_cluster : ProjectionCluster) :
    """Initialize a new export camera at the mean position of the active camera, 
    with the correct orientation, field of view and resolution."""

    scene = bpy.context.scene
    og_cam = scene.camera
    og_res = (bpy.context.scene.render.resolution_x, bpy.context.scene.render.resolution_y)
    print("Stored resolution to '%s': %dx%d"%(scene.name, og_res[0], og_res[1]))
    print('Camera FOV angle : %.3f°' % math.degrees(og_cam.data.angle))

    build_scene = core.scene_shift.get_build_scene()
    
    build_scene.original_resolution = og_res
    build_scene.original_camera = og_cam.name
    scene.render.resolution_percentage = 100

    collection = get_COLDEV(scene)
    
    depth = core.get_exported_cams_max_depth()
    render_cam_location = core.export_camera.get_camera_best_position(projection_cluster, depth=depth)

    export_cam = create_camera("Export_Camera", render_cam_location, collection)
    
    update_export_camera_aperture(export_cam, projection_cluster)
    export_cam.rotation_euler = (math.pi/2, 0, 0)

    scene.camera = export_cam


def simulate_aperture(w, h) :
    alpha = np.arctan(w/2)
    beta = np.arctan(h/2)

    return (-alpha, alpha, -beta, beta)



def get_extreme_frame_angles(aperture, projection_cluster, render_cam_location) :
    """Returns (min_angle, max_angle), two aperture values calculated from the 
    largest and tightest projection in the cluster.
    
    - projection_cluster : cf import_export.core.projection_clusters
    - render_cam_location : location of the export camera   """

    print("Calculating angle list.")

    area_list = []
    dim_list = []

    for p in projection_cluster.data :
        depth = abs(p.points[0][1] - render_cam_location.y)
        normalized_width = p.width() / depth
        normalized_height = p.height() / depth

        normalized_area = normalized_width * normalized_height

        area_list.append(normalized_area)
        dim_list.append((normalized_width, normalized_height))

    order = np.argsort(np.array(area_list))

    w_min, h_min = dim_list[order[0]]
    w_max, h_max = dim_list[order[-1]]

    min_angle = simulate_aperture(w_min, h_min)
    max_angle = simulate_aperture(w_max, h_max)

    print("Min angle : ", core.camera_angles.pretty_angle(min_angle))
    print("Max angle : ", core.camera_angles.pretty_angle(max_angle))

    return (min_angle, max_angle)


def update_export_camera_aperture(export_cam, projection_cluster : ProjectionCluster, tilt=False) :
    """Updates euler_rotation, resolution and field of view angle of 
    the camera to include every point viewed by active camera."""

    render_cam_location = export_cam.location

    aperture = core.camera_angles.get_aperture(projection_cluster, render_cam_location)
    min_angle, max_angle = get_extreme_frame_angles(aperture, projection_cluster, render_cam_location)
    
    print("\nAperture : ", aperture)
    
    aperture = core.export_camera.add_alpha_margins(aperture, max_angle, 0.2)
    res_X, res_Y = get_resolution(aperture, min_angle)

    print("New resolution : ", res_X, res_Y)

    bpy.context.scene.render.resolution_x = int(res_X)
    bpy.context.scene.render.resolution_y = int(res_Y)

    if res_X > res_Y :
        export_cam.data.angle = aperture[1]-aperture[0]
    else : 
        export_cam.data.angle = aperture[3]-aperture[2]

    if tilt :
        export_cam.rotation_euler[0] = math.pi/2+(aperture[3]+aperture[2])/2
        export_cam.rotation_euler[2] = (aperture[1]+aperture[0])/2


def remove_export_camera(export_camera) :
    """Removes export camera and restores original camera and resolution."""
    
    scene = core.scene_shift.get_build_scene()
    print("Original cam : ", scene.original_camera)
    print("Original res : %dx%d"%(scene.original_resolution[0], scene.original_resolution[1]))

    bpy.data.objects.remove(export_camera, do_unlink=True)
    scene.camera = bpy.data.objects[scene.original_camera]


def restore_resolution() :
    scene = core.scene_shift.get_build_scene()

    if scene.original_resolution is not None :
        bpy.context.scene.render.resolution_x = scene.original_resolution[0]
        bpy.context.scene.render.resolution_y = scene.original_resolution[1]


def get_export_camera() :
    build_scene = bpy.data.scenes["SCN_BUILD"]
    for o in build_scene.objects :
        if o.name == "Export_Camera" :
            return o


def restore_export_camera(path) :

    data = core.data_export.get_export_cam_data(path)
    scene = core.scene_shift.get_build_scene()

    scene.original_resolution = (bpy.context.scene.render.resolution_x, bpy.context.scene.render.resolution_y)
    scene.original_camera = scene.camera.name

    if "Export_Camera" in scene.objects.keys() :
        bpy.data.objects.remove(bpy.data.objects["Export_Camera"], do_unlink=True)

    coldev = get_COLDEV(scene)
    
    cam = create_camera("Export_Camera", data['cam_location'], coldev)

    bpy.context.scene.render.resolution_x = int(data["resolution"][0])
    bpy.context.scene.render.resolution_y = int(data["resolution"][1])

    cam.data.angle = data["cam_angle"]
    cam.rotation_euler = tuple(data["cam_rotation"])

    core.switch_scene(scene)
    scene.camera = cam