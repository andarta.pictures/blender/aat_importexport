# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import os

from .common.utils import register_classes, unregister_classes
from .common import log_3D

from . import core


initialized = False


class IO_Main_Panel(bpy.types.Panel):
    bl_label = "Andarta BG Toolbox"
    bl_idname = "OBJECT_PT_IO_mainpanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        global initialized

        if not initialized :
            log_3D.set_display(False)
            initialized = True

        layout = self.layout

        # row = layout.row()
        # row.operator("anim.gp_io_test", icon = "MODIFIER", text="CRASH INCOMING ?!")

        link_active = core.scene_split.link_active(context.scene)

        if link_active : 
            row = layout.row(align=True)
            row.operator("anim.simplified_ui_infos", icon="INFO", text="LINK ACTIVE")

        else : 
            row = layout.row(align=True)
            row.operator("object.build_decors", icon = "DUPLICATE", text="Split scenes")
            row.operator("object.clone_to_scene", icon = "LINK_BLEND", text="")
            
            row = layout.row(align=True)
            row.operator("anim.gp_io_export_cam_menu", icon = "PLUS", text="Create Export Cam")
            row.operator("anim.gp_io_update_export_camera", icon = "RECOVER_LAST", text="")

            icon = "DECORATE_KEYFRAME" if log_3D.get_display() else "DECORATE_ANIMATE"
            row.operator("anim.gp_display_3d_logs", icon = icon, text="")
            row.operator("anim.gp_io_restore_export_camera", icon = "IMPORT", text="")
            row.operator("anim.gp_io_cancel_export_camera", icon = "CANCEL", text="")
            
            row = layout.row()

            button_text = "Export visible objects"
            if core.render.render_status != "" :
                button_text = "Exporting " + core.render.render_status

            row.operator("anim.gp_io_render_export_camera", icon = "SCENE", text=button_text)
        
        group = layout.column(align=True)
        row = group.row()

        row.prop(context.scene, "assets_pack_list", text="")
        
        if link_active : 
            row = group.row(align=True)
            row.operator("anim.gp_io_refresh_plane_textures", icon = "FILE_REFRESH", text="Refresh textures")
        
        else :
            row = group.row(align=True)
            row.operator("anim.gp_io_link_assets", icon = "LOCKVIEW_ON", text="Link to Photoshop")
            row.operator("anim.gp_io_refresh_plane_textures", icon = "FILE_REFRESH", text="")



    

def available_assets_pack(self, context) :
    split_path = lambda path : os.path.normpath(path).split(os.sep)
    
    def sort_criterion(item) :
        elements = split_path(item[0])
        criterion = "%s_%s" % (elements[-3], elements[-2])
        criterion = criterion.replace("PAINT_BG", "001")
        criterion = criterion.replace("LAYOUT_BG", "002")
        return criterion
    
    def shot_infos(name) :
        pop = lambda l : l[0] if len(l) > 0 else "ERR"
        info = lambda tag : pop([e[len(tag):] for e in name.split("_") if e.startswith(tag)])
        return f"{info('SQ')}_{info('SH')}"

    asset_packs = core.texture_plane.get_available_asset_packs()
    blend_shot_info = shot_infos(os.path.basename(bpy.data.filepath))
    
    if len(asset_packs) == 0 :
        return [("NONE", "No available asset pack.", "No folder ending with '-assets' was found in the project folder")]
    
    asset_pack_list = []
    for asset_path in asset_packs :
        split = split_path(asset_path)
        asset_shot_infos = shot_infos(split[-1])
        if asset_shot_infos == blend_shot_info :
            asset_name = " / ".join(split[-3:])
            asset_pack_list.append((asset_path, asset_name, asset_path))
    
    asset_pack_list.sort(key=sort_criterion)
    # print("Sort criterion : ", [sort_criterion(item) for item in asset_pack_list])

    return asset_pack_list



classes = [
    IO_Main_Panel,
]

def register() :
    register_classes(classes)
    bpy.types.Scene.assets_pack_list = bpy.props.EnumProperty(items=available_assets_pack, default=None)


def unregister() :
    del bpy.types.Scene.assets_pack_list
    unregister_classes(classes)